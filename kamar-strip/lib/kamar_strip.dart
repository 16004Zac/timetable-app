import 'dart:async';
import 'dart:io';

import 'package:html/dom.dart';
import 'package:html/parser.dart' show parse;
import 'dart:convert' show UTF8;
import 'package:intl/intl.dart';



class ConfiguredHourMinute {
  int hour;
  int min;

  ConfiguredHourMinute();

  ConfiguredHourMinute convert(String time) {
    time = time.toLowerCase();
    String ext = 'am';

    if (time.endsWith('am') || time.endsWith('pm')) {
      ext = time.substring(time.length - 2, time.length);
      time = time.substring(0, time.length - 2);
    }

    List<String> parts = time.split(':');

    this.hour = int.parse(parts[0]);
    this.min = int.parse(parts[1]);
    if (ext == 'pm' && hour < 12) {
      hour += 12;
    }

    return this;
  }

  DateTime getTimeOffset(DateTime day) {
    return new DateTime(
      day.year,
      day.month,
      day.day,
      hour,
      min,
      day.second,
      day.millisecond,
      day.microsecond);
  }

  @override
  String toString() {
    return hour.toString().padLeft(2, "0") + ':' +
      min.toString().padLeft(2, "0");
  }
}

class ConfiguredTimeRange {
  ConfiguredHourMinute start;
  ConfiguredHourMinute end;

  ConfiguredTimeRange();

  ConfiguredTimeRange convert(String val) {
    List<String> startend = val.split('-');

    this.start = new ConfiguredHourMinute().convert(startend[0]);
    this.end = new ConfiguredHourMinute().convert(startend[1]);

    return this;
  }

  @override
  String toString() {
    return "[${start} - ${end}]";
  }
}

class ConfiguredTimeCollection {
  String day;
  List<ConfiguredTimeRange> range = [];

  ConfiguredTimeCollection convert(String day, String line) {
    this.day = day;

    line.split(',').forEach((it) {
      if (it == 'x') {
        range.add(null); // don't care
      } else {
        range.add(new ConfiguredTimeRange().convert(it));
      }
    });

    return this;
  }

  @override
  String toString() {
    return "day ${day} : range: ${range.toString()}";
  }
}


String hourMin(DateTime t) {
  return t.hour.toString().padLeft(2, '0') + ':' + t.minute.toString().padLeft(2, '0');
}

class Slot {
  String subject;
  String location;
  DateTime startTime;
  DateTime endTime;

  Slot({this.subject, this.location, this.startTime, this.endTime});

  @override
  String toString() {
    return "[subject: ${subject}, location: ${location}, time: ${hourMin(startTime)}-${hourMin(endTime)}]";
  }
}

class Day {
  DateTime day;
  List<Slot> slots = [];

  Day(this.day);

  @override
  String toString() {
    return "[day:${day} slots:${slots}]\n";
  }
}

class Week {
  int weekNo;
  List<Day> days = [];

  Week(this.weekNo);

  @override
  String toString() {
    return "Week: ${weekNo}, days=${days}";
  }
}

class ExtractTimetable {
  // ignore slots named these things
//  List<String> _ignorePrefix = ['interval', 'lunch', 'after school'];

// these are Glendowie slots
  Map<String, String> _defaults = {
    '4': '8:45am-9:30am,9:35am-11:05am,11:30am-12:15pm,x,12:20pm-1:05pm,1:45pm-2:30pm,x,2:35pm-3:20pm',
    '3': 'x,9:45am-10:05am,10:10am-11:10am,x,11:35am-12:35pm,12:40pm-1:40pm,x,2:20pm-3:20pm',
    'default': '8:45am-9:45am,9:50am-10:05am,10:10am-11:10am,x,11:35am-12:35pm,12:40pm-1:40pm,x,2:20pm-3:20pm'
  };

  List<ConfiguredTimeCollection> _timeCollection = [];

  ExtractTimetable() {
    // lets initialize the timeslots
    _defaults.forEach((key, val) {
      _timeCollection.add(new ConfiguredTimeCollection().convert(key, val));
    });
  }

  // given a specific date, we need to determine what day of the week it is and then
  // find the template for that weekday. Different days have different period structures.
  ConfiguredTimeCollection _findWeekDay(DateTime day) {
    String weekDay = day.weekday.toString();

    // find that day otherwise use default
    var matched = _timeCollection.where((it) => it.day == weekDay);

    if (matched.length == 0) {
      matched = _timeCollection.where((it) => it.day == 'default');
    }

    return matched.first;
  }

  // this picks up all of the days of the week off the  page and creates a Week
  // object that has empty days in them ready for filling
  void _collectDates(List<Element> daysElements, Week week) {
    DateTime now = new DateTime.now();

    int currentYear = now.year;

    daysElements.forEach((Element day) {
      var dayText = day.text.trim() + ' ' + currentYear.toString();
      DateTime actualDay = new DateFormat('dd MMM yyyy').parse(dayText);
      week.days.add(new Day(actualDay));
    });
  }

  // this just makes sure we are on a page with a week and then requests the
  // week to be filled
  Week _extractWeekInformationFromPage(Document timetablePage) {
    List<Element> daysElements = timetablePage.querySelectorAll('#timetable_table .tt_date');

    if (daysElements.length == 0) {
      return null;
    }

    Week week = new Week(int.parse(timetablePage.querySelector('#week').attributes['value']));

    _collectDates(daysElements, week);

    return week;
  }

  // this is the main function, it expects the timetable page as a string to
  // be passed to it
  Week extractTimetable(String body) {
    var timetablePage = parse(body);

    // now we have the known days of this week from Kamar's perspective
    Week week = _extractWeekInformationFromPage(timetablePage);

    bool found = true;
    int periodCounter = 1;

    List<List<Element>> offsets = [];

    // keep cycling until we run out of periods
    while (found) {
      // periods go sideways - so we get period 2 and all slots in the week in one .period_slot_2
      var periodElements = timetablePage.querySelectorAll('#timetable_table .period_slot_' + periodCounter.toString());

      if (periodElements.length > 0) {
        // do we have a timeslot for it?
        var firstCol = periodElements[0].querySelector('div.tt_time').innerHtml.trim();

        if (firstCol.length > 0) {
          offsets.add(periodElements);
        }
      } else {
        found = false;
      }

      periodCounter ++;
    }

    for(int counter = 0; counter < 5; counter ++) {
      DateTime day = week.days[counter].day;

      ConfiguredTimeCollection timeCollection = _findWeekDay(day);

      // walk DOWN the period list
      for(int slotCounter = 0; slotCounter < (offsets.length-1); slotCounter++) {
        ConfiguredTimeRange ctr = timeCollection.range[slotCounter];

        if (ctr != null) {
          Element cell = offsets[slotCounter][counter+1];
          String subject = cell.querySelector('strong').text;
          String location = cell.querySelector('.result').text?.trim();

          if (subject != null && subject.length > 0 && location != null && location.length > 0) {
            week.days[counter].slots.add(new Slot(subject: subject, location: location,
              startTime: ctr.start.getTimeOffset(day), endTime: ctr.end.getTimeOffset(day)));
          }
        }
      }
    }

    return week;
  }
}


class TimetableCollector {
  String host;
  HttpClient client;
  List<Cookie> sessionCookies;

  TimetableCollector(this.host);

  int currentWeek() {
    // lets figure out the current week
    DateTime now = new DateTime.now();
    // "1969-07-20 20:18:04Z"
    DateTime beginningOfYear = DateTime.parse(
      now.year.toString() + "-01-01 00:00:00Z");
    Duration diff = now.difference(beginningOfYear);
    return (diff.inDays / 7).round();
  }

  Future<String> _requestTimetable() async {
    var request = await client.getUrl(
      Uri.parse(host + 'timetable/' + currentWeek().toString()));
    sessionCookies.forEach((cookie) => request.cookies.add(cookie));
    request.headers.add('Origin', host);
    request.headers.add('User-Agent',
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36');
    var response = await request.close();

    var responseBody = await UTF8.decodeStream(response);

    new File("/tmp/timetable.html").writeAsString(responseBody).then((file) =>
      print("saved timetable for week")
    );

    return responseBody;
  }

  Future<String> _postUserData() async {
    // make the login request
    HttpClientRequest request = await client.postUrl(
      Uri.parse('${host}process-login'));

    sessionCookies.forEach((cookie) => request.cookies.add(cookie));
    request.headers.removeAll('user-agent');
    request.headers.removeAll('transfer-encoding');
    request.headers.contentType =
    new ContentType("application", "x-www-form-urlencoded", charset: "utf-8");
    request.headers.add('Origin', host);
    request.headers.add('User-Agent',
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36');
    request.headers
      .add(HttpHeaders.REFERER, host);
    String body = "username=14035&password=" + Uri.encodeFull('burtoN!7');
    request.contentLength = body.length;
    request.write(body);

    HttpClientResponse response = await request.close();

    print(response.statusCode);
    print(response.headers);

    var responseBody = await UTF8.decodeStream(response);
    print(responseBody);

    return await _requestTimetable();
  }

  Future<String> login() async {
    client = new HttpClient();

    var request = await client.getUrl(Uri.parse(host + 'login'));

    var response = await request.close();
    String body = null;

    if (response.statusCode == 200) {
      sessionCookies = response.cookies;
      body = await _postUserData();
    }

    client.close();

    return body;
  }
}





String parseHtml() {
  var document = parse(
    '<body>Hello world! <a>fred</a>  <a href="www.html5rocks.com">HTML5 rocks!</a></body>');
  return document
    .querySelector('a[href]')
    .innerHtml;
}
