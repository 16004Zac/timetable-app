import 'dart:async';
import 'dart:convert';


import 'package:flutter/material.dart';
import 'package:glendowie_ninja/models.dart';
import 'package:glendowie_ninja/serializers.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sembast/sembast_io.dart';
import 'package:sembast/sembast.dart';
import 'package:path_provider/path_provider.dart';

// using https://github.com/tekartik/sembast.dart

class NinjaDatabase {
  Database _database;
  
  Future<Database> initDatabase() async {

    var path = await getApplicationDocumentsDirectory();

    String dbPath = join(path.path, "timetable_ninja_v2.db");
    DatabaseFactory dbFactory = ioDatabaseFactory;

    _database = await dbFactory.openDatabase(dbPath);

    return new Future.value(_database);
  }

  Future<Database> _getDatabase() async {
    if (_database == null) {
      return initDatabase();
    }

    return _database;
  }

  // true if we have login state
  Future<bool> loginState() async {
    LoginSettings ls = await getLoginSettings();

    return ls != null;
  }

  final String LOGIN_SETTINGS = '/login-settings';
  final String SUBJECT_LIST = '/subject-list';
  final String SUBJECT_PREFIX = '/subject/';
  final String SUBJECT_SETTINGS_PREFIX = '/subject-settings/';

  Future<LoginSettings> getLoginSettings() async {
    Database db = await _getDatabase();

    String login = await db.get(LOGIN_SETTINGS);
    LoginSettings ls = null;
    if (login == null) {
      ls = null;
    } else {
      // this works because of the weird encoding style
      ls = serializers.deserialize(json.decode(login));
    }

    return ls;
  }

  Future<LoginSettings> saveLoginSettings(LoginSettings ls) async {
    Database db = await _getDatabase();

    await db.put(json.encode(serializers.serialize(ls)), LOGIN_SETTINGS);

    return ls;
  }

  Future<void> updateSubjects(List<Subject> subjects) async {
    Database db = await _getDatabase();

    List<String> subjectSummary = await _getSubjects(db);

    List<Future<dynamic>> saveMe = [];

    subjects.forEach((s) {
      // remove all the ones that we are adding, so we are only left with the
      // diff
      subjectSummary.remove(s.name);

      saveMe.add(
          db.put(json.encode(serializers.serialize(s)),
            SUBJECT_PREFIX + s.name));
    });

    subjectSummary.forEach((s) async {
      saveMe.add(db.delete(SUBJECT_PREFIX + s));
    });

    // save the new subject list
    var jsonSummary = json.encode(subjects.map((s) => s.name).toList(growable: false));
    saveMe.add(db.put(jsonSummary, SUBJECT_LIST));

    // return a future that waits for the saving of the data to occur
    return new Future<void>(() => Future.wait(saveMe));
  }

  var colours = [
    [Colors.red, Colors.black],
    [Colors.blue, Colors.white],
    [Colors.deepPurple, Colors.white],
    [Colors.yellow, Colors.black],
    [Colors.cyan, Colors.white],
    [Colors.green, Colors.white],
    [Colors.amber, Colors.white],
    [Colors.deepOrangeAccent, Colors.white],
    [Colors.black, Colors.white],
  ];


  Future<List<SubjectSettings>> getFullSubjectSettings() async {
    List<SubjectSettings> fullSubjects = [];
    Database db = await _getDatabase();

    List<String> subjects = await _getSubjects(db);

    int colorIndex = 0;
    List<Future<dynamic>> futures = [];
    subjects.forEach((s) {
      futures.add(
        _getSubjectSettings(s, db).then((subj) {
          if (subj == null) {
            fullSubjects.add(new SubjectSettings((ssb) => ssb
              ..name = s
              ..backgroundColour = colours[colorIndex][0].value
              ..foregroundColour = colours[colorIndex][1].value
            ));

            colorIndex ++;
          } else {
            fullSubjects.add(subj);
          }
        })
      );
    });
    
    await Future.wait(futures);

    // sort by name
    fullSubjects.sort((a, b) => a.name.compareTo(b.name));

    return fullSubjects;
  }

  Future<List<String>> _getSubjects(Database db) async {
    List<String> subjectSummary = [];

    try {
      String subjectsVal = await db.get(SUBJECT_LIST);
      if (subjectsVal != null) {
        List<dynamic> subjects = json.decode(subjectsVal);
        subjects.forEach((f) => subjectSummary.add(f.toString()));
      }
    } catch (e) {
    }

    return subjectSummary;
  }

  Future<List<String>> getSubjects() async {
    Database db = await _getDatabase();
    return _getSubjects(db);
  }

  Future<SubjectSettings> _getSubjectSettings(String subject, Database db) async {
    String subjectSettingsJson = await db.get(SUBJECT_SETTINGS_PREFIX + subject);
    if (subjectSettingsJson == null) {
      return null;
    }

    return serializers.deserialize(json.decode(subjectSettingsJson));
  }

  Future<Subject> _getSubject(String subject, Database db) async {
    String subjectJson = await db.get(SUBJECT_PREFIX + subject);
    if (subjectJson == null) {
      return null;
    }

    return serializers.deserialize(json.decode(subjectJson));
  }

  Future<Subject> getSubject(String subject) async {
    Database db = await _getDatabase();
    return _getSubject(subject, db);
  }

  DateFormat _dayFormatter = new DateFormat('yyyy-MM-dd');

  String _dayKey(DateTime dt) {
    return '/day/' + _dayFormatter.format(dt);
  }

  Future<void> updateDays(List<Day> days) async {
    Database db = await _getDatabase();

    List<Future<dynamic>> saveMe = [];

    days.forEach((d) {
      String key = _dayKey(d.day);
      saveMe.add(
          db.put(
              json.encode(serializers.serialize(d)), key)
      );
    } );

    return new Future<void>(() => Future.wait(saveMe));
  }

  Future<Day> getDay(DateTime viewDate) async {
    Database db = await _getDatabase();

    var key = _dayKey(viewDate);
    String day = await db.get(key);

    if (day != null) {
      return serializers.deserialize(json.decode(day));
    } else {
      return null;
    }
  }

  Future<void> updateTerms(Terms terms) async {
    Database db = await _getDatabase();
    await db.put(json.encode(serializers.serialize(terms)), '/terms');
  }

  Future<Terms> getTerms() async {
    Database db = await _getDatabase();
    return serializers.deserialize(json.decode(await db.get('/terms')));
  }

  static const currentHomeworkKey = '/homework/current';
  static const oldHomeworkKey = '/homework/old';

  Future<List<Homework>> getCurrentHomework() async {
    List<Homework> items = await _getHomework(currentHomeworkKey);

    await _checkOutdatedHomework(items);

    return items;
  }

  Future<List<Homework>> getOldHomework() async {
    return _getHomework(oldHomeworkKey);
  }

  Future<List<Homework>> _getFakeHomework(String key) async {
    List<Homework> homework = [];

    homework.add(Homework((b) => b
        ..id = '1234'
        ..whenDue = new DateTime.now().add(Duration(days: 2))
        ..title = 'English assignment'
        ..subject = '13ENG'
        ..description = 'Some desc'
        ..homeworkSize = 3
        ..value = 3
    ));
    homework.add(Homework((b) => b
      ..id = '1235'
      ..whenDue = new DateTime.now().add(Duration(days: 3))
      ..title = 'Math assignment'
      ..subject = '13MCA'
      ..description = 'Some desc'
      ..homeworkSize = 3
      ..value = 3
    ));
    homework.add(Homework((b) => b
      ..id = '1239'
      ..whenDue = new DateTime.now().add(Duration(days: 3))
      ..title = 'Meth assignment'
      ..subject = '13MCA'
      ..description = 'Some desc'
      ..homeworkSize = 3
      ..value = 3
    ));
    homework.add(Homework((b) => b
      ..id = '1236'
      ..whenDue = new DateTime.now().add(Duration(days: 10))
      ..title = 'History assignment'
      ..subject = '13HIS'
      ..description = 'Some desc'
      ..homeworkSize = 3
      ..value = 3
    ));
    homework.add(Homework((b) => b
      ..id = '1237'
      ..whenDue = new DateTime.now().add(Duration(days: 12))
      ..title = 'History assignment 2'
      ..subject = '13HIS'
      ..description = 'Ut deserunt ground round ex ad duis chicken est dolore picanha tempor. Jowl meatball bacon sunt. In ex eu mollit pastrami cupidatat ut tri-tip venison turkey kevin. Picanha labore turkey commodo. Sunt landjaeger minim, in kielbasa nisi spare ribs occaecat qui ipsum pork loin elit veniam.'
      ..homeworkSize = 3
      ..value = 3
    ));
    homework.add(Homework((b) => b
      ..id = '1240'
      ..whenDue = new DateTime.now().add(Duration(days: 12))
      ..title = 'Meth assignment'
      ..subject = '13MCA'
      ..description = 'Biltong quis anim meatball commodo tempor mollit flank salami ipsum aliquip nostrud. Ribeye sint ipsum, labore fugiat beef ribs brisket short loin do. Prosciutto esse burgdoggen occaecat dolor. Adipisicing veniam chuck t-bone, nulla culpa sausage officia cow shoulder hamburger pig ullamco ad velit. Fatback flank beef brisket venison est incididunt doner prosciutto ribeye eu.'
      ..homeworkSize = 3
      ..value = 3
    ));
    homework.add(Homework((b) => b
      ..id = '1238'
      ..whenDue = new DateTime.now().add(Duration(days: 17))
      ..title = 'History assignment 17'
      ..subject = '13HIS'
      ..description = 'Some desc'
      ..homeworkSize = 3
      ..value = 3
    ));
    homework.add(Homework((b) => b
      ..id = '1230'
      ..whenDue = new DateTime.now().add(Duration(days: 12))
      ..title = 'English assignment'
      ..subject = '13ENG'
      ..description = 'Some desc'
      ..homeworkSize = 3
      ..value = 3
    ));

    return homework;
  }

  Future<List<Homework>> _getHomework(String key) async {
    Database db = await _getDatabase();

    List<Homework> homework = [];
    String val = await db.get(key);
    if (val != null) {
      List<String> dateOrderedHomework = json.decode(val);
      dateOrderedHomework.forEach((hw) {
        homework.add(serializers.deserialize(json.decode(hw)));
      });
    } else {
      homework = await _getFakeHomework(null);
    }

    return homework;
  }

  void _saveHomework(List<Homework> items, String key) async {
    // save all in order
    List<String> savedHomework = items.map((hw) => json.encode(serializers.serialize(hw)));

    Database db = await _getDatabase();
    db.put(json.encode(savedHomework), key);
  }

  void _insertOldHomework(List<Homework> old) async {
    Database db = await _getDatabase();

    List<String> dateOrderedHomework = [];
    String val = await db.get(oldHomeworkKey);

    if (val != null) {
      dateOrderedHomework = json.decode(val);
    }

    old.reversed.forEach((hw) {
      dateOrderedHomework.insert(0, json.encode(serializers.serialize(hw)));
    });

    await db.put(json.encode(dateOrderedHomework), oldHomeworkKey);
  }

  void _checkOutdatedHomework(List<Homework> items) async {
    DateTime now = DateTime.now();

    List<Homework> removed = [];
    for(var hw in items) {
      if (hw.whenDue.isBefore(now)) {
        removed.add(hw);
      } else {
        break;
      }
    }

    // were there any to remove? if so, remove them from the items list & save them
    // in the old's list
    if (removed.length > 0) {
      removed.forEach((it) => items.remove(it));
      await _insertOldHomework(removed);
    }
  }

  Future<List<Homework>> saveHomework(Homework h) async {
    // get the current set
    List<Homework> items = await getCurrentHomework();

    // update or insert
    int pos = items.indexWhere((el) => el.id == h.id, -1);

    if (pos == -1) {
      items.add(h);
    } else {
      items[pos] = h;
    }

    // sort earliest first
    items.sort((a, b) => a.whenDue.compareTo(b.whenDue));

    _checkOutdatedHomework(items);

    await _saveHomework(items, currentHomeworkKey);

    return items;
  }
}

