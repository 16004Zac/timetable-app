
//import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:xml/xml.dart' as xml;

import 'package:glendowie_ninja/models.dart';
import 'package:xml/xml/nodes/document.dart';
import 'package:xml/xml/nodes/parent.dart';

class _Day {
  DateTime day;
  int term;
  int termWeek;
  int week;
  DayStatusType dayStatusType;

  List<Period> periods = [];

  _Day({this.day, this.term, this.termWeek, this.week, this.dayStatusType});

  @override
  String toString() {
    return "Date ${day} in school week ${week}, term ${term} - type ${dayStatusType}\n. Periods are: ${periods}";
  }
}

class _Subject {
  final String subject;
  List<DateTime> days = [];

  _Subject(this.subject);

  @override
  String toString() {
    return "Subject: `${subject}` - days ${days}";
  }
}

class DayList {
  List<Day> days = [];
  List<Subject> subjects;
  Terms terms;
  String formTeacher;
  String level;
  String studentId;

  @override
  String toString() {
    return """Form Teacher: ${formTeacher}, Level ${level}, ID: ${studentId},\n"
    Days: ${days}
    Subjects: ${subjects}
""";
  }
}

class Login {
  int errorCode;
  bool success;
  int loginLevel;
  String studentId;
  String key;
  String error;

  @override
  String toString() {
    return "errorCode ${errorCode}, success ${success}, loginLevel ${loginLevel}, studentId ${studentId}";
  }


}

class DailyInfo {
  Map<String, List<_Day>> weeks;
  Terms terms;
}


class KamarAdapter {
  /*
    [        ] I/flutter ( 4831): <LogonResults apiversion="2.4.2">
[        ] I/flutter ( 4831): <ErrorCode>0</ErrorCode><Error>Incorrect Username or Password</Error>
[        ] I/flutter ( 4831): </LogonResults>
[   +9 ms] E/flutter ( 4831): [ERROR:topaz/lib/tonic/logging/dart_error.cc(16)] Unhandled exception:
[        ] E/flutter ( 4831): Bad state: No element
[        ] E/flutter ( 4831): #0      Iterable.first (dart:core/iterable.dart:535)
[        ] E/flutter ( 4831): #1      MappedIterable.first (dart:_internal/iterable.dart:371)
[        ] E/flutter ( 4831): #2      KamarAdapter.transformLogin (package:glendowie_ninja/kamar_adapater.dart:78:56)
[        ] E/flutter ( 4831): #3      KamarApi.login (package:glendowie_ninja/kamar_api.dart:45:34)
[        ] E/flutter ( 4831): <asynchronous suspension>
[        ] E/flutter ( 4831): #4      _NinjaLoginPageState._login.<anonymous closure> (/data/user/0/com.bluetrainsoftware.timetable_ninja/cache/glendowie_ninjaUOEWEN/glendowie_ninja/lib/page_login.dart:53:20)
[        ] E/flutter ( 4831): #5      State.setState (package:flutter/src/widgets/framework.dart:1108:30)
[        ] E/flutter ( 4831): #6      _NinjaLoginPageState._login (/data/user/0/com.bluetrainsoftware.timetable_ninja/cache/glendowie_ninjaUOEWEN/glendowie_ninja/lib/page_login.dart:51:7)
[        ] E/flutter ( 4831): #7      _NinjaLoginPageState.build.<anonymous closure> (/data/user/0/com.bluetrainsoftware.timetable_ninja/cache/glendowie_ninjaUOEWEN/glendowie_ninja/lib/page_login.dart:113:44)
[
     */

  static String _findXmlString(XmlDocument doc, String node) {
    var found = doc.findAllElements(node);
    if (found.length > 0) {
      return found.first.text;
    } else {
      return null;
    }
  }

  static int _findXmlInt(XmlDocument doc, String node) {
    var found = doc.findAllElements(node);
    if (found.length > 0) {
      return int.parse(found.first.text);
    } else {
      return -1;
    }
  }

  static Login transformLogin(String loginXml) {
    var login = xml.parse(loginXml);
    Login result = new Login();

    print(loginXml);
    result.errorCode = _findXmlInt(login, 'ErrorCode');
    result.success = _findXmlString(login, 'Success')== 'YES';
    result.loginLevel = _findXmlInt(login, 'LogonLevel');
    result.studentId = _findXmlString(login, 'CurrentStudent');
    result.key = _findXmlString(login, 'Key');
    result.error = _findXmlString(login, 'Error');

    return result;
  }

  static DayList transform(String calendarXml, String timetableXml) {
    DailyInfo dailyInfo = _mapCalendar(calendarXml);
    Map<String, List<_Day>> weeks = dailyInfo.weeks;
    Map<String, _Subject> subjects = {};

    DayList dayList = new DayList();

    var timetable = xml.parse(timetableXml);

    var student = timetable.findAllElements('Student');
    if (student.length == 1) {
       XmlParent sNode = student.first as XmlParent;
       dayList.studentId = sNode.findElements('IDNumber').first.text;
       dayList.level = sNode.findElements('Level').first.text;
       dayList.formTeacher = sNode.findElements('Tutor').first.text;
       sNode.findElements('TimetableData').first.children.forEach((wk) {
         // not interested in non-XmlElements, all the rest is cr/lfs
         if (wk is xml.XmlElement) {
           xml.XmlElement week = wk as xml.XmlElement;

           List<_Day> days = weeks[week.name.local.substring(1)];

           if (days != null) {
             week.children.forEach((d) {
               if (d is xml.XmlElement) {
                 xml.XmlElement day = d as xml.XmlElement;
                 int dayOfWeek = int.parse(day.name.local.substring(1));
                 if (dayOfWeek <= days.length) { // make sure we have a week for that
                   // kamar start their week on a sunday (i.e. d0 is a sunday for a week, so d1 is a monday)
                   _Day weekDay = days[dayOfWeek];

                   _decodePeriods(weekDay, subjects, day.text);

                 }
               }
             });
           } 
         }

       });
    }

    dayList.days = _transformDays(weeks);
    subjects.remove('Form');
    dayList.subjects = _transformSubjects(subjects.values);

    dayList.terms = dailyInfo.terms;

    return dayList;
  }

  static void _decodePeriods(_Day weekDay, Map<String, _Subject> subjects, String codedPeriods) {
    List<String> parts = codedPeriods.split('|');
    // the 0th element is "Term-OpenStatus", where OpenStatus = 0 (holiday), 10 - open day, 1-6 - cycle day
    // we don't care about it
    if (parts.length > 0) {
      if (parts[0] != '0-0') { // holiday
        parts.removeAt(0);

        int periodCounter = 0;
        parts.forEach((period) {
          // in order these fields are: grid type, line identifier, subject code, teacher, room
          // we throw away grid type and line identifier as they aren't used
          List<String> parts = period.split("-");
          if (parts.length == 5 && parts[0].length > 0) {
            Period p = new Period((b) => b
                ..subject = parts[2]
                ..teacher = parts[3]
                ..room = parts[4]
                ..slot = periodCounter
            );

            weekDay.periods.add(p);

            // now tell the subject it is on this day
            _Subject s = subjects[p.subject];
            if (s == null) {
              s = new _Subject(p.subject);
              subjects[p.subject] = s;
            }

            s.days.add(weekDay.day);
          }

          periodCounter ++;
        });
      }
    }
  }

  static DailyInfo _mapCalendar(String calendarXml) {
    Map<String, List<_Day>> weeks = {};
    Map<String,int> termWeeks = new Map<String,int>();
    DailyInfo info = new DailyInfo();
    info.weeks = weeks;

    var calendar = xml.parse(calendarXml);

    var xmlDays = calendar.findAllElements('Day');
    if (xmlDays.length >= 0) {
      xmlDays.forEach((dayChild) {
        XmlParent day = dayChild as XmlParent;
        var status = day.findElements('Status').first.text.toLowerCase();
        if (status == 'holidays') {
          status = 'holiday';
        }

        DateTime dayOfYear = DateTime.parse(
            day.findElements('Date').first.text + " 00:00:00Z");
        var term = int.parse(day.findElements('TermA').first.text);
        var week = day.findElements('WeekYear').first.text;
        var termWeekText = day.findElements('WeekA').first.text;
        var termWeek = 0;
        if (termWeekText != null && termWeekText.length > 0) {
          termWeek = int.parse(termWeekText);
          var sTerm = term.toString();

          if (termWeeks[sTerm] != null) {
            if (termWeeks[sTerm] < termWeek) {
              termWeeks[sTerm] = termWeek;
            }
          } else {
            termWeeks[sTerm] = termWeek;
          }
        }

        if (week == null || week.length == 0) {
          return;
        }

        List<_Day> days = weeks[week];
        if (days == null) {
          days = [];
          weeks[week] = days;
        }

        days.add(new _Day(day: dayOfYear, term: term, termWeek: termWeek,
            week: int.parse(week),
            dayStatusType: DayStatusType.valueOf(status)));

      });
    }

    info.terms = new Terms((b) => b ..terms = new MapBuilder<String,int>(termWeeks));

    return info;
  }

  static List<Day> _transformDays(Map<String, List<_Day>> days) {
    List<Day> savedDays = [];

    days.forEach((String week, List<_Day> days) {
      int dayOfWeek = 1;
      days.forEach((dow) {
        Day d = new Day((b) => b
            ..day = dow.day
            ..term = dow.term
            ..termWeek = dow.termWeek
            ..dayOfWeek = dayOfWeek
            ..periods = new SetBuilder<Period>(dow.periods)
            ..dayStatusType = dow.dayStatusType
            ..week = dow.week
        );

        savedDays.add(d);

        dayOfWeek ++;
      });
    });


    return savedDays;
//    days.values.map((_day) =>  new Day((b) => b
//      ..day = _day.day
//      ..dayOfWeek = 1
//      ..periods = _day.periods
//
//    ))
//    return [];
  }

  static List<Subject> _transformSubjects(Iterable<_Subject> values) {
    return values.map((s) => new Subject((b) => b
        ..name = s.subject
        ..days = new SetBuilder<DateTime>(s.days)
    )).toList(growable: false);
  }
}

//void main() async {
//  String calendarXml = await new File('test/calendar.xml').readAsString();
//  String timetableXml = await new File('test/timetable-result.xml').readAsString();
//
//  DayList dl = KamarAdapter.transform(calendarXml, timetableXml);
//
//  print(dl.terms);
//}