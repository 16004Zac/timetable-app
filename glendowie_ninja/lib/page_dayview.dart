import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/component_day.dart';
import 'package:glendowie_ninja/component_drawer.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/models.dart';
import 'package:glendowie_ninja/time_peroids.dart';
import 'package:intl/intl.dart';

class NinjaDayViewPage extends StatefulWidget {
  final NinjaDatabase db;
  final TimePeriods timePeriods;
  final FirebaseAnalytics analytics;

  NinjaDayViewPage({Key key, this.db, this.timePeriods, this.analytics}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _NinjaDayViewPageState();
  }
}

// the day can be null
class _DayData {
  int index;
  Day day;

  _DayData(this.day, this.index);
}

class _NinjaDayViewPageState extends State<NinjaDayViewPage> {
  DateFormat dateFormat = new DateFormat('EEEE dd MMMM');
  int _daysInYear;
  DateTime _startOfYear;
  PageController pageController;
  Map<int, _DayData> days = {};
  Terms _terms;

  _NinjaDayViewPageState() {
    var now = new DateTime.now();
    var endOfYear = new DateTime(now.year, 12, 31);
    _startOfYear = new DateTime(now.year, 1, 1);

    _daysInYear = endOfYear.difference(_startOfYear).inDays;

//    print("days in year $_daysInYear}");
//    print("today is ${now
//        .difference(_startOfYear)
//        .inDays}");

    pageController = new PageController(
        initialPage: now.difference(_startOfYear).inDays + 1);
  }

  @override
  void initState() {
    super.initState();
//    print("init state");
    // check if they haven't logged in before and send them there
    widget.db.loginState().then((loggedIn) {
      if (!loggedIn) {
        Navigator.pushNamed(context, '/login');
      } else {
        widget.db.getTerms().then((terms) {
//          print("got terms");
          setState(() {
            _terms = terms;
          });
        });
      }
    });
  }

  AppBar _buildAppBar() {
    return new AppBar(
      elevation: 0.0,
      title: new Text('Your Day'),
    );
  }

  Widget createDay(BuildContext context, int index) {
    var currentDay = _startOfYear.add(new Duration(days: index));

    if (days[index] == null) {
      widget.db.getDay(currentDay).then((day) {
        if (days[index] == null) {
          setState(() {
            var dayData = new _DayData(day, index);

            days[index] = dayData;
          });
        }
      });

      return new Container();
    } else {
      return new DayView(currentDay, days[index].day, widget.timePeriods);
    }
  }

  Widget createDayViewPage(BuildContext context, int index) {
    widget.analytics.setCurrentScreen(screenName: 'yourday', screenClassOverride: 'Day${index}');

    var heading = new Padding(
      padding: new EdgeInsets.all(4.0),
      child: new Row(
        children: <Widget>[
          new Expanded(
            child: new Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      dateFormat
                          .format(_startOfYear.add(new Duration(days: index))),
                      style: new TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18.0),
                    ),
                    new Text(_getWeek(index))
                  ],
                )),
          ),
          new FlatButton(
              child: const Icon(Icons.today),

              onPressed: () {
                pageController.jumpToPage(
                    new DateTime.now().difference(_startOfYear).inDays + 1);
              }),
        ],
      ),
    );

    return new Column(children: <Widget>[
      heading,
      new Expanded(child: createDay(context, index))
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: _buildAppBar(),
      drawer: new NinjaDrawer(widget.db),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new PageView.builder(
            controller: pageController,
            itemBuilder: (BuildContext context, int index) =>
                createDayViewPage(context, index),
            itemCount: _daysInYear,
          )),
    );
  }

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  String _getWeek(int index) {
    var currentDay = days[index];
    if (_terms == null || currentDay == null) {
      return '';
    } else {
      String weekTotal = '?';
      if (_terms != null &&
          _terms.terms[currentDay.day.term.toString()] != null) {
        weekTotal = _terms.terms[currentDay.day.term.toString()].toString();
      }
      return "Term ${currentDay.day.term}, Week ${currentDay.day
          .termWeek} of ${weekTotal}";
    }
  }
}
