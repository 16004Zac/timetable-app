

class ConfiguredHourMinute implements Comparable<ConfiguredHourMinute> {
  int hour;
  int min;

  ConfiguredHourMinute();

  ConfiguredHourMinute convert(String time) {
    time = time.toLowerCase();
    String ext = 'am';

    if (time.endsWith('am') || time.endsWith('pm')) {
      ext = time.substring(time.length - 2, time.length);
      time = time.substring(0, time.length - 2);
    }

    List<String> parts = time.split(':');

    this.hour = int.parse(parts[0]);
    this.min = int.parse(parts[1]);
    if (ext == 'pm' && hour < 12) {
      hour += 12;
    }

    return this;
  }

  ConfiguredHourMinute dupe({int offset = 0}) {
    ConfiguredHourMinute chm = new ConfiguredHourMinute();

    int minutes = (hour * 60) + min - offset;
    chm.hour = minutes ~/ 60;
    chm.min = minutes % 60;

    return chm;
  }

  int inMinutes() {
    return hour * 60 + min;
  }

  DateTime getTimeOffset(DateTime day) {
    return new DateTime(
        day.year,
        day.month,
        day.day,
        hour,
        min,
        day.second,
        day.millisecond,
        day.microsecond);
  }

  @override
  String toString() {
    return hour.toString().padLeft(2, "0") + ':' +
        min.toString().padLeft(2, "0");
  }

  @override
  int compareTo(ConfiguredHourMinute other) {
    if (hour < other.hour || (hour == other.hour && min < other.min)) {
      return -1;
    }

    if (other.hour == hour && other.min == min) {
      return 0;
    }

    return 1;
  }
}

class ConfiguredTimeRange  {
  ConfiguredHourMinute start;
  ConfiguredHourMinute end;
  String _name;


  ConfiguredTimeRange();

  ConfiguredTimeRange.spacer(String nm) {
    this._name = nm;
  }

  int get inMinutes => end.inMinutes() - start.inMinutes();

  ConfiguredTimeRange.convert(String val) {
    List<String> startend = val.split('-');

    this.start = new ConfiguredHourMinute().convert(startend[0]);
    this.end = new ConfiguredHourMinute().convert(startend[1]);
  }

  bool isSpacer() => _name != null;

  void set name(String n) => _name = n;
  String get name => _name != null ? _name : "${start} - ${end}";

  @override
  String toString() {
    return "${_name == null ? '':_name}: ${start} - ${end}";
  }
}

final _digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

class ConfiguredTimeCollection {
  String day;
  List<ConfiguredTimeRange> range = [];

  ConfiguredTimeCollection.convert(String day, String line) {
    this.day = day;

    ConfiguredTimeRange previousTimeRange = null;
    ConfiguredHourMinute previousEndTime = null;

    line.split(',').forEach((String it) {
      if (_digits.contains(it.substring(0, 1))) {
        var convertedTimeRange = new ConfiguredTimeRange.convert(it);
        range.add(convertedTimeRange);

        if (previousTimeRange != null && previousTimeRange.end == null) {
          previousTimeRange.end = convertedTimeRange.start.dupe(offset: 1);
        }

        previousEndTime = convertedTimeRange.end;
        previousTimeRange = convertedTimeRange;
      } else {
        ConfiguredTimeRange spacer = new ConfiguredTimeRange.spacer(it);
        if (previousEndTime != null) {
          spacer.start = previousEndTime.dupe(offset: -1);
        }
        range.add(spacer); // don't care
        previousEndTime = null;
        previousTimeRange = spacer;
      }
    });
  }

  @override
  String toString() {
    return "day ${day} : range: ${range.toString()}";
  }

  void adjustForEarliestTime(ConfiguredHourMinute earliest) {
    if (range.length > 0 && range[0].start == null) {
      range[0].start = earliest;
    }
  }
}

abstract class TimePeriods {
  List<ConfiguredTimeCollection> _timeCollection = [];

  Map<String, String> getDefaults();

  TimePeriods() {
    // lets initialize the timeslots
    getDefaults().forEach((key, val) {
      _timeCollection.add(new ConfiguredTimeCollection.convert(key, val));
    });

    var earliest = earliestTime();

    _timeCollection.forEach((ctc) {
      ctc.adjustForEarliestTime(earliest);
    });
  }

  ConfiguredHourMinute earliestTime() {
    ConfiguredHourMinute earliest = null;

    _timeCollection.forEach((ctc) {
      if (earliest == null || (ctc.range[0].start != null && ctc.range[0].start.compareTo(earliest) < 0)) {
        earliest = ctc.range[0].start;
      }
    });

    return earliest;
  }

  ConfiguredHourMinute lastTime() {
    ConfiguredHourMinute latest = null;

    _timeCollection.forEach((ctc) {
      if (latest == null || ctc.range[ctc.range.length-1].end.compareTo(latest) > 0) {
        latest = ctc.range[0].start;
      }
    });

    return latest;
  }


// given a specific date, we need to determine what day of the week it is and then
// find the template for that weekday. Different days have different period structures.
  ConfiguredTimeCollection findWeekDay(DateTime day) {
    String weekDay = day.weekday.toString();

    // find that day otherwise use default
    var matched = _timeCollection.where((it) => it.day == weekDay);

    if (matched.length == 0) {
      matched = _timeCollection.where((it) => it.day == 'default');
    }

    return matched.first;
  }
}

class GlendowieTime extends TimePeriods {
  Map<String, String> _defaults = {
    '4': '8:45am-9:30am,9:35am-11:05am,Interval,11:30am-12:15pm,12:20pm-1:05pm,Lunch,1:45pm-2:30pm,2:35pm-3:20pm',
    '3': 'Teachers Only,9:45am-10:05am,10:10am-11:10am,Interval,11:35am-12:35pm,12:40pm-1:40pm,Lunch,2:20pm-3:20pm',
    'default': '8:45am-9:45am,9:50am-10:05am,10:10am-11:10am,Interval,11:35am-12:35pm,12:40pm-1:40pm,Lunch,2:20pm-3:20pm'
  };

  @override
  Map<String, String> getDefaults() {
    return _defaults;
  }
}

void main() {
  TimePeriods tp = new GlendowieTime();

  for (var value in tp._timeCollection) {
    print(value.toString());
  }
}