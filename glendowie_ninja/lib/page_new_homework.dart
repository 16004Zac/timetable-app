import 'dart:async';


import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/models.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

class NinjaNewHomeworkViewPage extends StatefulWidget {
  final NinjaDatabase db;
  final FirebaseAnalytics analytics;
  Homework homework;

  NinjaNewHomeworkViewPage({Key key, this.db, this.analytics, this.homework})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _NinjaNewHomeworkViewPageState();
  }
}

class _InputDropdown extends StatelessWidget {
  const _InputDropdown({
    Key key,
    this.child,
    this.labelText,
    this.valueText,
    this.valueStyle,
    this.onPressed }) : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: onPressed,
      child: new InputDecorator(
        decoration: new InputDecoration(
          labelText: labelText,
        ),
        baseStyle: valueStyle,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text(valueText, style: valueStyle),
            new Icon(Icons.arrow_drop_down,
                color: Theme.of(context).brightness == Brightness.light ? Colors.grey.shade700 : Colors.white70
            ),
          ],
        ),
      ),
    );
  }
}

class _DateTimePicker extends StatelessWidget {
  const _DateTimePicker({
    Key key,
    this.labelText,
    this.selectedDate,
    this.selectedTime,
    this.selectDate,
    this.selectTime
  }) : super(key: key);

  final String labelText;
  final DateTime selectedDate;
  final TimeOfDay selectedTime;
  final ValueChanged<DateTime> selectDate;
  final ValueChanged<TimeOfDay> selectTime;

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: new DateTime(new DateTime.now().year, new DateTime.now().month),
        lastDate: new DateTime(2101)
    );
    if (picked != null && picked != selectedDate)
      selectDate(picked);
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: selectedTime
    );
    if (picked != null && picked != selectedTime)
      selectTime(picked);
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle valueStyle = TextStyle(fontSize: 17.0);
    return new Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        new Expanded(
          flex: 4,
          child: new _InputDropdown(
            labelText: labelText,
            valueText: new DateFormat.yMMMd().format(selectedDate),
            valueStyle: valueStyle,
            onPressed: () { _selectDate(context); },
          ),
        ),
        const SizedBox(width: 12.0),
        new Expanded(
          flex: 3,
          child: new _InputDropdown(
            valueText: selectedTime.format(context),
            valueStyle: valueStyle,
            onPressed: () { _selectTime(context); },
          ),
        ),
      ],
    );
  }
}

class _NinjaNewHomeworkViewPageState extends State<NinjaNewHomeworkViewPage> {
  DateTime _dueDate;
  TimeOfDay _dueTime;
  String _subject;
  String _description;
  String _title;
  int _size;
  double _value;
  List<String> _subjects = [];
  List<String> _sizing = ['Trump Hands (S)', '1.5L Bottle of Coke (M)', 'North Korean Nuke (L)', 'Trumps Ego (XL)'];
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  
  AppBar _buildAppBar() {
    return new AppBar(
      elevation: 0.0,
      title: new Text((widget.homework == null ? 'New' : 'Edit') + ' homework'),
    );
  }

  @override
  void initState() {
    super.initState();

    widget.db.getSubjects().then((s) {
      setState(() {
        _subjects = s;
        if (_subject == null) {
          _subject = _subjects[0];
        }
      });
    });

    _size = widget.homework?.homeworkSize ?? 0;
    _dueDate = widget.homework?.whenDue ?? new DateTime.now();
    _dueTime = widget.homework == null ? const TimeOfDay(hour: 7, minute: 28) : TimeOfDay.fromDateTime(widget.homework.whenDue);
    _subject = widget.homework?.subject;
    _description = widget.homework?.description ?? '';
    _value = widget.homework?.value?.toDouble() ?? 4.0;
  }

  Widget subjectAndSize() {
    return new Row(
      children: <Widget>[
        Expanded(
          flex: 2,
          child: new InputDecorator(
            decoration: const InputDecoration(
              labelText: 'Subject',
            ),
            isEmpty: _subject == null,
            child: new DropdownButton<String>(
              value: _subject,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  _subject = newValue;
                });
              },
              items: _subjects.map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        ),
        SizedBox(width: 12.0),
        Expanded(
          flex: 3,
          child: InputDecorator(
            decoration: const InputDecoration(
              labelText: 'Size',
            ),
            isEmpty: _size == null,
            child: new DropdownButton<String>(
              value: _size == null ? null : _sizing[_size],
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  _size = _sizing.indexOf(newValue);
                });
              },

              items: _sizing.map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        )
      ],
    );
  }

  Widget sizeSlider() {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Row(
        children: <Widget>[
          Text("Value/Credits:", style: TextStyle(fontSize: 18.0),),
          Slider(value: _value, onChanged: (val) {
            setState(() => _value = val);
          }, min: 1.0, max: 12.0, divisions: 11, ),
          Text("${_value.toInt().toString()}", style: TextStyle(fontSize: 18.0),)
        ],
      ),
    );
  }

  Widget homeworkBody() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        children: <Widget>[
          new TextFormField(
            keyboardType: TextInputType.text,
            decoration: const InputDecoration(
              labelText: 'Title',
            ),
            controller: new TextEditingController(text: _title),
            validator: (val) => (val == null || val.length == 0) ? 'Title cannot be empty' : null,
            onFieldSubmitted: (val) {
              _title = val;
            },
          ),
          subjectAndSize(),
          new _DateTimePicker(
            labelText: 'Due',
            selectedDate: _dueDate,
            selectedTime: _dueTime,
            selectDate: (DateTime date) {
              setState(() {
                _dueDate = date;
              });
            },
            selectTime: (TimeOfDay time) {
              setState(() {
                _dueTime = time;
              });
            },
          ),
          sizeSlider(),
          Padding(
            padding: const EdgeInsets.only(top: 12.0, bottom: 8.0),
            child: Container(
                child: new Text('Description', style: TextStyle(fontSize: 18.0),)),
          ),
          new TextFormField(maxLines: 20,
          controller: new TextEditingController(text: _description),
          onFieldSubmitted: (val) => _description = val,)
        ],
      ),
    );
  }

  Homework _recreateHomework() {
    DateTime dt = new DateTime(_dueDate.year, _dueDate.month, _dueDate.day, _dueTime.hour, _dueTime.minute);

    Homework hw = new Homework((b) => b
      ..id = widget.homework == null ? Uuid().v4() : widget.homework.id
      ..description = _description
      ..subject = _subject
      ..title = _title
      ..whenDue = dt
    );

    return hw;
  }

  Widget _wrapper() {
    Form form;

    form = Form(
      key: formKey,
      child: new Column(
        children: <Widget>[
          new Expanded(child: homeworkBody()),
          new Container(height: 60.0,
          alignment: Alignment.centerRight,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: new MaterialButton(onPressed: () {
              formKey.currentState.validate();
              //form.
            }, child: new Text('Save'), color: Theme.of(context).buttonColor,),
          )
          )
        ],
      ),
    );

    return form;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: _buildAppBar(),
        body: _wrapper());
  }
}

