// GENERATED CODE - DO NOT MODIFY BY HAND

part of ninja_model;

// **************************************************************************
// Generator: BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_returning_this
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first

const DayStatusType _$open = const DayStatusType._('open');
const DayStatusType _$weekend = const DayStatusType._('weekend');
const DayStatusType _$holiday = const DayStatusType._('holiday');

DayStatusType _$dstValueOf(String name) {
  switch (name) {
    case 'open':
      return _$open;
    case 'weekend':
      return _$weekend;
    case 'holiday':
      return _$holiday;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<DayStatusType> _$dstValues =
    new BuiltSet<DayStatusType>(const <DayStatusType>[
  _$open,
  _$weekend,
  _$holiday,
]);

Serializer<LoginSettings> _$loginSettingsSerializer =
    new _$LoginSettingsSerializer();
Serializer<Period> _$periodSerializer = new _$PeriodSerializer();
Serializer<DayStatusType> _$dayStatusTypeSerializer =
    new _$DayStatusTypeSerializer();
Serializer<Day> _$daySerializer = new _$DaySerializer();
Serializer<Terms> _$termsSerializer = new _$TermsSerializer();
Serializer<SubjectSettings> _$subjectSettingsSerializer =
    new _$SubjectSettingsSerializer();
Serializer<Subject> _$subjectSerializer = new _$SubjectSerializer();
Serializer<Homework> _$homeworkSerializer = new _$HomeworkSerializer();

class _$LoginSettingsSerializer implements StructuredSerializer<LoginSettings> {
  @override
  final Iterable<Type> types = const [LoginSettings, _$LoginSettings];
  @override
  final String wireName = 'LoginSettings';

  @override
  Iterable serialize(Serializers serializers, LoginSettings object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'username',
      serializers.serialize(object.username,
          specifiedType: const FullType(String)),
      'password',
      serializers.serialize(object.password,
          specifiedType: const FullType(String)),
    ];
    if (object.schoolUrl != null) {
      result
        ..add('schoolUrl')
        ..add(serializers.serialize(object.schoolUrl,
            specifiedType: const FullType(String)));
    }
    if (object.schoolName != null) {
      result
        ..add('schoolName')
        ..add(serializers.serialize(object.schoolName,
            specifiedType: const FullType(String)));
    }
    if (object.schoolLogo != null) {
      result
        ..add('schoolLogo')
        ..add(serializers.serialize(object.schoolLogo,
            specifiedType: const FullType(String)));
    }
    if (object.studentId != null) {
      result
        ..add('studentId')
        ..add(serializers.serialize(object.studentId,
            specifiedType: const FullType(String)));
    }
    if (object.level != null) {
      result
        ..add('level')
        ..add(serializers.serialize(object.level,
            specifiedType: const FullType(String)));
    }
    if (object.formTeacher != null) {
      result
        ..add('formTeacher')
        ..add(serializers.serialize(object.formTeacher,
            specifiedType: const FullType(String)));
    }

    return result;
  }

  @override
  LoginSettings deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new LoginSettingsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'username':
          result.username = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'password':
          result.password = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'schoolUrl':
          result.schoolUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'schoolName':
          result.schoolName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'schoolLogo':
          result.schoolLogo = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'studentId':
          result.studentId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'level':
          result.level = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'formTeacher':
          result.formTeacher = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$PeriodSerializer implements StructuredSerializer<Period> {
  @override
  final Iterable<Type> types = const [Period, _$Period];
  @override
  final String wireName = 'Period';

  @override
  Iterable serialize(Serializers serializers, Period object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'slot',
      serializers.serialize(object.slot, specifiedType: const FullType(int)),
      'teacher',
      serializers.serialize(object.teacher,
          specifiedType: const FullType(String)),
      'subject',
      serializers.serialize(object.subject,
          specifiedType: const FullType(String)),
      'room',
      serializers.serialize(object.room, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Period deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new PeriodBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'slot':
          result.slot = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'teacher':
          result.teacher = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'subject':
          result.subject = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'room':
          result.room = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$DayStatusTypeSerializer implements PrimitiveSerializer<DayStatusType> {
  @override
  final Iterable<Type> types = const <Type>[DayStatusType];
  @override
  final String wireName = 'DayStatusType';

  @override
  Object serialize(Serializers serializers, DayStatusType object,
          {FullType specifiedType: FullType.unspecified}) =>
      object.name;

  @override
  DayStatusType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType: FullType.unspecified}) =>
      DayStatusType.valueOf(serialized as String);
}

class _$DaySerializer implements StructuredSerializer<Day> {
  @override
  final Iterable<Type> types = const [Day, _$Day];
  @override
  final String wireName = 'Day';

  @override
  Iterable serialize(Serializers serializers, Day object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'week',
      serializers.serialize(object.week, specifiedType: const FullType(int)),
      'dayOfWeek',
      serializers.serialize(object.dayOfWeek,
          specifiedType: const FullType(int)),
      'term',
      serializers.serialize(object.term, specifiedType: const FullType(int)),
      'termWeek',
      serializers.serialize(object.termWeek,
          specifiedType: const FullType(int)),
      'day',
      serializers.serialize(object.day,
          specifiedType: const FullType(DateTime)),
      'dayStatusType',
      serializers.serialize(object.dayStatusType,
          specifiedType: const FullType(DayStatusType)),
    ];
    if (object.periods != null) {
      result
        ..add('periods')
        ..add(serializers.serialize(object.periods,
            specifiedType:
                const FullType(BuiltSet, const [const FullType(Period)])));
    }

    return result;
  }

  @override
  Day deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new DayBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'week':
          result.week = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'dayOfWeek':
          result.dayOfWeek = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'term':
          result.term = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'termWeek':
          result.termWeek = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'day':
          result.day = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'dayStatusType':
          result.dayStatusType = serializers.deserialize(value,
              specifiedType: const FullType(DayStatusType)) as DayStatusType;
          break;
        case 'periods':
          result.periods.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltSet, const [const FullType(Period)]))
              as BuiltSet);
          break;
      }
    }

    return result.build();
  }
}

class _$TermsSerializer implements StructuredSerializer<Terms> {
  @override
  final Iterable<Type> types = const [Terms, _$Terms];
  @override
  final String wireName = 'Terms';

  @override
  Iterable serialize(Serializers serializers, Terms object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'terms',
      serializers.serialize(object.terms,
          specifiedType: const FullType(
              BuiltMap, const [const FullType(String), const FullType(int)])),
    ];

    return result;
  }

  @override
  Terms deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new TermsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'terms':
          result.terms.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(String),
                const FullType(int)
              ])) as BuiltMap);
          break;
      }
    }

    return result.build();
  }
}

class _$SubjectSettingsSerializer
    implements StructuredSerializer<SubjectSettings> {
  @override
  final Iterable<Type> types = const [SubjectSettings, _$SubjectSettings];
  @override
  final String wireName = 'SubjectSettings';

  @override
  Iterable serialize(Serializers serializers, SubjectSettings object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'backgroundColour',
      serializers.serialize(object.backgroundColour,
          specifiedType: const FullType(int)),
      'foregroundColour',
      serializers.serialize(object.foregroundColour,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SubjectSettings deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new SubjectSettingsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'backgroundColour':
          result.backgroundColour = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'foregroundColour':
          result.foregroundColour = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SubjectSerializer implements StructuredSerializer<Subject> {
  @override
  final Iterable<Type> types = const [Subject, _$Subject];
  @override
  final String wireName = 'Subject';

  @override
  Iterable serialize(Serializers serializers, Subject object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'days',
      serializers.serialize(object.days,
          specifiedType:
              const FullType(BuiltSet, const [const FullType(DateTime)])),
    ];

    return result;
  }

  @override
  Subject deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new SubjectBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'days':
          result.days.replace(serializers.deserialize(value,
              specifiedType: const FullType(
                  BuiltSet, const [const FullType(DateTime)])) as BuiltSet);
          break;
      }
    }

    return result.build();
  }
}

class _$HomeworkSerializer implements StructuredSerializer<Homework> {
  @override
  final Iterable<Type> types = const [Homework, _$Homework];
  @override
  final String wireName = 'Homework';

  @override
  Iterable serialize(Serializers serializers, Homework object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'whenDue',
      serializers.serialize(object.whenDue,
          specifiedType: const FullType(DateTime)),
      'subject',
      serializers.serialize(object.subject,
          specifiedType: const FullType(String)),
      'title',
      serializers.serialize(object.title,
          specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'homeworkSize',
      serializers.serialize(object.homeworkSize,
          specifiedType: const FullType(int)),
      'value',
      serializers.serialize(object.value, specifiedType: const FullType(int)),
    ];
    if (object.completed != null) {
      result
        ..add('completed')
        ..add(serializers.serialize(object.completed,
            specifiedType: const FullType(bool)));
    }

    return result;
  }

  @override
  Homework deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new HomeworkBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'whenDue':
          result.whenDue = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'subject':
          result.subject = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'homeworkSize':
          result.homeworkSize = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'completed':
          result.completed = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$LoginSettings extends LoginSettings {
  @override
  final String username;
  @override
  final String password;
  @override
  final String schoolUrl;
  @override
  final String schoolName;
  @override
  final String schoolLogo;
  @override
  final String studentId;
  @override
  final String level;
  @override
  final String formTeacher;

  factory _$LoginSettings([void updates(LoginSettingsBuilder b)]) =>
      (new LoginSettingsBuilder()..update(updates)).build();

  _$LoginSettings._(
      {this.username,
      this.password,
      this.schoolUrl,
      this.schoolName,
      this.schoolLogo,
      this.studentId,
      this.level,
      this.formTeacher})
      : super._() {
    if (username == null)
      throw new BuiltValueNullFieldError('LoginSettings', 'username');
    if (password == null)
      throw new BuiltValueNullFieldError('LoginSettings', 'password');
  }

  @override
  LoginSettings rebuild(void updates(LoginSettingsBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  LoginSettingsBuilder toBuilder() => new LoginSettingsBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! LoginSettings) return false;
    return username == other.username &&
        password == other.password &&
        schoolUrl == other.schoolUrl &&
        schoolName == other.schoolName &&
        schoolLogo == other.schoolLogo &&
        studentId == other.studentId &&
        level == other.level &&
        formTeacher == other.formTeacher;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, username.hashCode), password.hashCode),
                            schoolUrl.hashCode),
                        schoolName.hashCode),
                    schoolLogo.hashCode),
                studentId.hashCode),
            level.hashCode),
        formTeacher.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LoginSettings')
          ..add('username', username)
          ..add('password', password)
          ..add('schoolUrl', schoolUrl)
          ..add('schoolName', schoolName)
          ..add('schoolLogo', schoolLogo)
          ..add('studentId', studentId)
          ..add('level', level)
          ..add('formTeacher', formTeacher))
        .toString();
  }
}

class LoginSettingsBuilder
    implements Builder<LoginSettings, LoginSettingsBuilder> {
  _$LoginSettings _$v;

  String _username;
  String get username => _$this._username;
  set username(String username) => _$this._username = username;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  String _schoolUrl;
  String get schoolUrl => _$this._schoolUrl;
  set schoolUrl(String schoolUrl) => _$this._schoolUrl = schoolUrl;

  String _schoolName;
  String get schoolName => _$this._schoolName;
  set schoolName(String schoolName) => _$this._schoolName = schoolName;

  String _schoolLogo;
  String get schoolLogo => _$this._schoolLogo;
  set schoolLogo(String schoolLogo) => _$this._schoolLogo = schoolLogo;

  String _studentId;
  String get studentId => _$this._studentId;
  set studentId(String studentId) => _$this._studentId = studentId;

  String _level;
  String get level => _$this._level;
  set level(String level) => _$this._level = level;

  String _formTeacher;
  String get formTeacher => _$this._formTeacher;
  set formTeacher(String formTeacher) => _$this._formTeacher = formTeacher;

  LoginSettingsBuilder();

  LoginSettingsBuilder get _$this {
    if (_$v != null) {
      _username = _$v.username;
      _password = _$v.password;
      _schoolUrl = _$v.schoolUrl;
      _schoolName = _$v.schoolName;
      _schoolLogo = _$v.schoolLogo;
      _studentId = _$v.studentId;
      _level = _$v.level;
      _formTeacher = _$v.formTeacher;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoginSettings other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$LoginSettings;
  }

  @override
  void update(void updates(LoginSettingsBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$LoginSettings build() {
    final _$result = _$v ??
        new _$LoginSettings._(
            username: username,
            password: password,
            schoolUrl: schoolUrl,
            schoolName: schoolName,
            schoolLogo: schoolLogo,
            studentId: studentId,
            level: level,
            formTeacher: formTeacher);
    replace(_$result);
    return _$result;
  }
}

class _$Period extends Period {
  @override
  final int slot;
  @override
  final String teacher;
  @override
  final String subject;
  @override
  final String room;

  factory _$Period([void updates(PeriodBuilder b)]) =>
      (new PeriodBuilder()..update(updates)).build();

  _$Period._({this.slot, this.teacher, this.subject, this.room}) : super._() {
    if (slot == null) throw new BuiltValueNullFieldError('Period', 'slot');
    if (teacher == null)
      throw new BuiltValueNullFieldError('Period', 'teacher');
    if (subject == null)
      throw new BuiltValueNullFieldError('Period', 'subject');
    if (room == null) throw new BuiltValueNullFieldError('Period', 'room');
  }

  @override
  Period rebuild(void updates(PeriodBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  PeriodBuilder toBuilder() => new PeriodBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Period) return false;
    return slot == other.slot &&
        teacher == other.teacher &&
        subject == other.subject &&
        room == other.room;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, slot.hashCode), teacher.hashCode), subject.hashCode),
        room.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Period')
          ..add('slot', slot)
          ..add('teacher', teacher)
          ..add('subject', subject)
          ..add('room', room))
        .toString();
  }
}

class PeriodBuilder implements Builder<Period, PeriodBuilder> {
  _$Period _$v;

  int _slot;
  int get slot => _$this._slot;
  set slot(int slot) => _$this._slot = slot;

  String _teacher;
  String get teacher => _$this._teacher;
  set teacher(String teacher) => _$this._teacher = teacher;

  String _subject;
  String get subject => _$this._subject;
  set subject(String subject) => _$this._subject = subject;

  String _room;
  String get room => _$this._room;
  set room(String room) => _$this._room = room;

  PeriodBuilder();

  PeriodBuilder get _$this {
    if (_$v != null) {
      _slot = _$v.slot;
      _teacher = _$v.teacher;
      _subject = _$v.subject;
      _room = _$v.room;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Period other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Period;
  }

  @override
  void update(void updates(PeriodBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Period build() {
    final _$result = _$v ??
        new _$Period._(
            slot: slot, teacher: teacher, subject: subject, room: room);
    replace(_$result);
    return _$result;
  }
}

class _$Day extends Day {
  @override
  final int week;
  @override
  final int dayOfWeek;
  @override
  final int term;
  @override
  final int termWeek;
  @override
  final DateTime day;
  @override
  final DayStatusType dayStatusType;
  @override
  final BuiltSet<Period> periods;

  factory _$Day([void updates(DayBuilder b)]) =>
      (new DayBuilder()..update(updates)).build();

  _$Day._(
      {this.week,
      this.dayOfWeek,
      this.term,
      this.termWeek,
      this.day,
      this.dayStatusType,
      this.periods})
      : super._() {
    if (week == null) throw new BuiltValueNullFieldError('Day', 'week');
    if (dayOfWeek == null)
      throw new BuiltValueNullFieldError('Day', 'dayOfWeek');
    if (term == null) throw new BuiltValueNullFieldError('Day', 'term');
    if (termWeek == null) throw new BuiltValueNullFieldError('Day', 'termWeek');
    if (day == null) throw new BuiltValueNullFieldError('Day', 'day');
    if (dayStatusType == null)
      throw new BuiltValueNullFieldError('Day', 'dayStatusType');
  }

  @override
  Day rebuild(void updates(DayBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  DayBuilder toBuilder() => new DayBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Day) return false;
    return week == other.week &&
        dayOfWeek == other.dayOfWeek &&
        term == other.term &&
        termWeek == other.termWeek &&
        day == other.day &&
        dayStatusType == other.dayStatusType &&
        periods == other.periods;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, week.hashCode), dayOfWeek.hashCode),
                        term.hashCode),
                    termWeek.hashCode),
                day.hashCode),
            dayStatusType.hashCode),
        periods.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Day')
          ..add('week', week)
          ..add('dayOfWeek', dayOfWeek)
          ..add('term', term)
          ..add('termWeek', termWeek)
          ..add('day', day)
          ..add('dayStatusType', dayStatusType)
          ..add('periods', periods))
        .toString();
  }
}

class DayBuilder implements Builder<Day, DayBuilder> {
  _$Day _$v;

  int _week;
  int get week => _$this._week;
  set week(int week) => _$this._week = week;

  int _dayOfWeek;
  int get dayOfWeek => _$this._dayOfWeek;
  set dayOfWeek(int dayOfWeek) => _$this._dayOfWeek = dayOfWeek;

  int _term;
  int get term => _$this._term;
  set term(int term) => _$this._term = term;

  int _termWeek;
  int get termWeek => _$this._termWeek;
  set termWeek(int termWeek) => _$this._termWeek = termWeek;

  DateTime _day;
  DateTime get day => _$this._day;
  set day(DateTime day) => _$this._day = day;

  DayStatusType _dayStatusType;
  DayStatusType get dayStatusType => _$this._dayStatusType;
  set dayStatusType(DayStatusType dayStatusType) =>
      _$this._dayStatusType = dayStatusType;

  SetBuilder<Period> _periods;
  SetBuilder<Period> get periods =>
      _$this._periods ??= new SetBuilder<Period>();
  set periods(SetBuilder<Period> periods) => _$this._periods = periods;

  DayBuilder();

  DayBuilder get _$this {
    if (_$v != null) {
      _week = _$v.week;
      _dayOfWeek = _$v.dayOfWeek;
      _term = _$v.term;
      _termWeek = _$v.termWeek;
      _day = _$v.day;
      _dayStatusType = _$v.dayStatusType;
      _periods = _$v.periods?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Day other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Day;
  }

  @override
  void update(void updates(DayBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Day build() {
    _$Day _$result;
    try {
      _$result = _$v ??
          new _$Day._(
              week: week,
              dayOfWeek: dayOfWeek,
              term: term,
              termWeek: termWeek,
              day: day,
              dayStatusType: dayStatusType,
              periods: _periods?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'periods';
        _periods?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Day', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Terms extends Terms {
  @override
  final BuiltMap<String, int> terms;

  factory _$Terms([void updates(TermsBuilder b)]) =>
      (new TermsBuilder()..update(updates)).build();

  _$Terms._({this.terms}) : super._() {
    if (terms == null) throw new BuiltValueNullFieldError('Terms', 'terms');
  }

  @override
  Terms rebuild(void updates(TermsBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  TermsBuilder toBuilder() => new TermsBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Terms) return false;
    return terms == other.terms;
  }

  @override
  int get hashCode {
    return $jf($jc(0, terms.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Terms')..add('terms', terms))
        .toString();
  }
}

class TermsBuilder implements Builder<Terms, TermsBuilder> {
  _$Terms _$v;

  MapBuilder<String, int> _terms;
  MapBuilder<String, int> get terms =>
      _$this._terms ??= new MapBuilder<String, int>();
  set terms(MapBuilder<String, int> terms) => _$this._terms = terms;

  TermsBuilder();

  TermsBuilder get _$this {
    if (_$v != null) {
      _terms = _$v.terms?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Terms other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Terms;
  }

  @override
  void update(void updates(TermsBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Terms build() {
    _$Terms _$result;
    try {
      _$result = _$v ?? new _$Terms._(terms: terms.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'terms';
        terms.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Terms', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$SubjectSettings extends SubjectSettings {
  @override
  final String name;
  @override
  final int backgroundColour;
  @override
  final int foregroundColour;

  factory _$SubjectSettings([void updates(SubjectSettingsBuilder b)]) =>
      (new SubjectSettingsBuilder()..update(updates)).build();

  _$SubjectSettings._({this.name, this.backgroundColour, this.foregroundColour})
      : super._() {
    if (name == null)
      throw new BuiltValueNullFieldError('SubjectSettings', 'name');
    if (backgroundColour == null)
      throw new BuiltValueNullFieldError('SubjectSettings', 'backgroundColour');
    if (foregroundColour == null)
      throw new BuiltValueNullFieldError('SubjectSettings', 'foregroundColour');
  }

  @override
  SubjectSettings rebuild(void updates(SubjectSettingsBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SubjectSettingsBuilder toBuilder() =>
      new SubjectSettingsBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! SubjectSettings) return false;
    return name == other.name &&
        backgroundColour == other.backgroundColour &&
        foregroundColour == other.foregroundColour;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, name.hashCode), backgroundColour.hashCode),
        foregroundColour.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SubjectSettings')
          ..add('name', name)
          ..add('backgroundColour', backgroundColour)
          ..add('foregroundColour', foregroundColour))
        .toString();
  }
}

class SubjectSettingsBuilder
    implements Builder<SubjectSettings, SubjectSettingsBuilder> {
  _$SubjectSettings _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  int _backgroundColour;
  int get backgroundColour => _$this._backgroundColour;
  set backgroundColour(int backgroundColour) =>
      _$this._backgroundColour = backgroundColour;

  int _foregroundColour;
  int get foregroundColour => _$this._foregroundColour;
  set foregroundColour(int foregroundColour) =>
      _$this._foregroundColour = foregroundColour;

  SubjectSettingsBuilder();

  SubjectSettingsBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _backgroundColour = _$v.backgroundColour;
      _foregroundColour = _$v.foregroundColour;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SubjectSettings other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$SubjectSettings;
  }

  @override
  void update(void updates(SubjectSettingsBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$SubjectSettings build() {
    final _$result = _$v ??
        new _$SubjectSettings._(
            name: name,
            backgroundColour: backgroundColour,
            foregroundColour: foregroundColour);
    replace(_$result);
    return _$result;
  }
}

class _$Subject extends Subject {
  @override
  final String name;
  @override
  final BuiltSet<DateTime> days;

  factory _$Subject([void updates(SubjectBuilder b)]) =>
      (new SubjectBuilder()..update(updates)).build();

  _$Subject._({this.name, this.days}) : super._() {
    if (name == null) throw new BuiltValueNullFieldError('Subject', 'name');
    if (days == null) throw new BuiltValueNullFieldError('Subject', 'days');
  }

  @override
  Subject rebuild(void updates(SubjectBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  SubjectBuilder toBuilder() => new SubjectBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Subject) return false;
    return name == other.name && days == other.days;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, name.hashCode), days.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Subject')
          ..add('name', name)
          ..add('days', days))
        .toString();
  }
}

class SubjectBuilder implements Builder<Subject, SubjectBuilder> {
  _$Subject _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  SetBuilder<DateTime> _days;
  SetBuilder<DateTime> get days => _$this._days ??= new SetBuilder<DateTime>();
  set days(SetBuilder<DateTime> days) => _$this._days = days;

  SubjectBuilder();

  SubjectBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _days = _$v.days?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Subject other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Subject;
  }

  @override
  void update(void updates(SubjectBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Subject build() {
    _$Subject _$result;
    try {
      _$result = _$v ?? new _$Subject._(name: name, days: days.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'days';
        days.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Subject', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Homework extends Homework {
  @override
  final String id;
  @override
  final DateTime whenDue;
  @override
  final String subject;
  @override
  final String title;
  @override
  final String description;
  @override
  final int homeworkSize;
  @override
  final int value;
  @override
  final bool completed;

  factory _$Homework([void updates(HomeworkBuilder b)]) =>
      (new HomeworkBuilder()..update(updates)).build();

  _$Homework._(
      {this.id,
      this.whenDue,
      this.subject,
      this.title,
      this.description,
      this.homeworkSize,
      this.value,
      this.completed})
      : super._() {
    if (id == null) throw new BuiltValueNullFieldError('Homework', 'id');
    if (whenDue == null)
      throw new BuiltValueNullFieldError('Homework', 'whenDue');
    if (subject == null)
      throw new BuiltValueNullFieldError('Homework', 'subject');
    if (title == null) throw new BuiltValueNullFieldError('Homework', 'title');
    if (description == null)
      throw new BuiltValueNullFieldError('Homework', 'description');
    if (homeworkSize == null)
      throw new BuiltValueNullFieldError('Homework', 'homeworkSize');
    if (value == null) throw new BuiltValueNullFieldError('Homework', 'value');
  }

  @override
  Homework rebuild(void updates(HomeworkBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  HomeworkBuilder toBuilder() => new HomeworkBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! Homework) return false;
    return id == other.id &&
        whenDue == other.whenDue &&
        subject == other.subject &&
        title == other.title &&
        description == other.description &&
        homeworkSize == other.homeworkSize &&
        value == other.value &&
        completed == other.completed;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, id.hashCode), whenDue.hashCode),
                            subject.hashCode),
                        title.hashCode),
                    description.hashCode),
                homeworkSize.hashCode),
            value.hashCode),
        completed.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Homework')
          ..add('id', id)
          ..add('whenDue', whenDue)
          ..add('subject', subject)
          ..add('title', title)
          ..add('description', description)
          ..add('homeworkSize', homeworkSize)
          ..add('value', value)
          ..add('completed', completed))
        .toString();
  }
}

class HomeworkBuilder implements Builder<Homework, HomeworkBuilder> {
  _$Homework _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  DateTime _whenDue;
  DateTime get whenDue => _$this._whenDue;
  set whenDue(DateTime whenDue) => _$this._whenDue = whenDue;

  String _subject;
  String get subject => _$this._subject;
  set subject(String subject) => _$this._subject = subject;

  String _title;
  String get title => _$this._title;
  set title(String title) => _$this._title = title;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  int _homeworkSize;
  int get homeworkSize => _$this._homeworkSize;
  set homeworkSize(int homeworkSize) => _$this._homeworkSize = homeworkSize;

  int _value;
  int get value => _$this._value;
  set value(int value) => _$this._value = value;

  bool _completed;
  bool get completed => _$this._completed;
  set completed(bool completed) => _$this._completed = completed;

  HomeworkBuilder();

  HomeworkBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _whenDue = _$v.whenDue;
      _subject = _$v.subject;
      _title = _$v.title;
      _description = _$v.description;
      _homeworkSize = _$v.homeworkSize;
      _value = _$v.value;
      _completed = _$v.completed;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Homework other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$Homework;
  }

  @override
  void update(void updates(HomeworkBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$Homework build() {
    final _$result = _$v ??
        new _$Homework._(
            id: id,
            whenDue: whenDue,
            subject: subject,
            title: title,
            description: description,
            homeworkSize: homeworkSize,
            value: value,
            completed: completed);
    replace(_$result);
    return _$result;
  }
}
