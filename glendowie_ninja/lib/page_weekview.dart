import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/component_drawer.dart';
import 'package:glendowie_ninja/models.dart';
import 'package:glendowie_ninja/time_peroids.dart';

class NinjaWeekViewPage extends StatefulWidget {
  final NinjaDatabase db;
  final TimePeriods timePeriods;
  final FirebaseAnalytics analytics;

  NinjaWeekViewPage({Key key, this.db, this.timePeriods, this.analytics})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _NinjaWeekViewPageState();
  }
}

class _NinjaWeekViewPageState extends State<NinjaWeekViewPage> {
  List<Day> _days = [];
  Map<String, SubjectSettings> _subjects = {};
  static const double pixelsPerBlock = 1.0;
  static const double blockWidth = 60.0;
  ConfiguredHourMinute earliestTime;
  ConfiguredHourMinute lastTime;

  @override
  void initState() {
    super.initState();
    // check if they haven't logged in before and send them there
    widget.db.loginState().then((loggedIn) {
      if (!loggedIn) {
        Navigator.pushNamed(context, '/login');
      } else {
        widget.analytics.setCurrentScreen(screenName: 'yourweek');

        DateTime now = new DateTime.now();
        DateTime sow = now.subtract(Duration(days: now.weekday - 1));

        List<Day> days = [null, null, null, null, null];
        List<Future<Day>> futures = [];
        for (int count = 0; count < 5; count++) {
          int localCount = count;
          futures.add(widget.db
              .getDay(sow.add(Duration(days: count)))
              .then((d) => days[localCount] = d));
        }

        earliestTime = widget.timePeriods.earliestTime();
        lastTime = widget.timePeriods.lastTime();

        futures.add(widget.db.getFullSubjectSettings().then((s) {
          print("got subjects ${s}");
          s.forEach((subj) => setState(() => _subjects[subj.name] = subj));
        }));

        Future.wait(futures).then((f) {
          setState(() => _days = days);
        });
      }
    });
  }

  AppBar _buildAppBar() {
    return new AppBar(
        elevation: 0.0,
        title: new Text('Your Week'),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.settings),
            tooltip: 'Settings',
            onPressed: _settings,
          ),
        ]);
  }

  void _settings() {
    Navigator.pushNamed(context, '/settings');
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: _buildAppBar(),
        drawer: new NinjaDrawer(widget.db),
        body: new Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _buildBody()),
        ));
  }

  var weekDays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

  Widget heading(String text) {
    return new Padding(
        padding: EdgeInsets.all(8.0),
        child: new Center(
            child: new Text(
          text,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        )));
  }

  Widget mondayHours(Day day) {
    List<Widget> cols = [];
    var tcDay = widget.timePeriods.findWeekDay(day.day);

    cols.add(heading(''));

    ConfiguredHourMinute lastEnd = null;
    tcDay.range.forEach((slot) {
      if (lastEnd != null) {
        int minutePadding = slot.start.inMinutes() - (lastEnd.inMinutes() + 1);
        if (minutePadding > 0) {
          cols.add(new SizedBox(height: minutePadding / pixelsPerBlock));
        }
      }

      lastEnd = slot.end;

      // if this is like, interval or lunch, just add a blank box
      if (slot.isSpacer()) {
        cols.add(new SizedBox(
          height: slot.inMinutes / pixelsPerBlock,
        ));
      } else {
        cols.add(new SizedBox(
            height: slot.inMinutes / pixelsPerBlock,
            child: new Container(
                height: slot.inMinutes / pixelsPerBlock,
                child: Align(
                  alignment: Alignment.topRight,
                  child: Text(
                    slot.start.toString(),
                    style: TextStyle(fontSize: 12.0),
                  ),
                ))));
      }
    });

    return new Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: cols,
      ),
    );
  }

  List<Widget> _buildBody() {
    List<Widget> widgets = [];

    if (_days.length > 0) {
      print("building widget, days :${_days.length}");

      widgets.add(mondayHours(_days[0]));

      _days.forEach((day) {
        if (day.periods == null || day.periods.length == 0) {
//          print("skipped day");
          return;
        } 

        List<Widget> cols = [];

        cols.add(heading(weekDays[day.day.weekday - 1]));

        int periodSlot = 0;

        // find the number of actual time slots we have today, some of which are
        // periods
        var tcDay = widget.timePeriods.findWeekDay(day.day);
        ConfiguredHourMinute lastEnd = null;
        tcDay.range.forEach((slot) {
          if (lastEnd != null) {
            int minutePadding =
                slot.start.inMinutes() - (lastEnd.inMinutes() + 1);
            if (minutePadding > 0) {
              cols.add(new SizedBox(height: minutePadding / pixelsPerBlock));
            }
          }

          lastEnd = slot.end;

          // if this is like, interval or lunch, just add a blank box
          if (slot.isSpacer()) {
            cols.add(new SizedBox(
              height: slot.inMinutes / pixelsPerBlock,
            ));
           } else if (periodSlot < day.periods.length) {
            var period = day.periods.elementAt(periodSlot);
            var subject = period.subject;
            if (subject.startsWith('1')) {
              subject = subject.substring(2);
            }
            periodSlot++;

            cols.add(new SizedBox(
                height: slot.inMinutes / pixelsPerBlock,
                child: new Container(
                    height: slot.inMinutes / pixelsPerBlock,
                    color: _background(period.subject),
                    child: new Center(
                      child: new Text(
                        subject,
                        style: TextStyle(
                            color: _foreground(period.subject),
                            fontWeight: FontWeight.bold),
                      ),
                    ))));
          }
        });

        widgets.add(Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: cols,
          ),
        ));
      });
    }

    return widgets;
  }
  
  Color _background(String subject) {
    if (subject == 'Form') {
      return Colors.blueGrey;
    }

    SubjectSettings s = _subjects[subject];
    return s == null ? Colors.white : Color(s.backgroundColour);
  }

  Color _foreground(String subject) {
    if (subject == 'Form') {
      return Colors.white;
    }
    
    SubjectSettings s = _subjects[subject];
    return s == null ? Colors.black : Color(s.foregroundColour);
  }
}
