import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/kamar_adapater.dart';
import 'package:glendowie_ninja/models.dart';


class KamarApi {
  HttpClient client;
  final String host;
  final NinjaDatabase db;
  String key;
  String studentId;

  Login _login;

  KamarApi(this.host, this.db) {
    this.client = new HttpClient();
  }

  Login getLogin() {
    return _login;
  }

  Future<HttpClientResponse> command(String body) async {
    HttpClientRequest request = await client.postUrl(
        Uri.parse('${host}/api/api.php'));

    request.headers.removeAll('user-agent');
    request.headers.removeAll('transfer-encoding');
    request.headers.contentType =
    new ContentType("application", "x-www-form-urlencoded", charset: "utf-8");
    request.headers.add('Origin', host);
    request.headers.add('User-Agent',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36');
    request.headers.add(HttpHeaders.REFERER, host);

    request.contentLength = body.length;
    request.write(body);

    return request.close();
  }

  Future<bool> login(String username, String password) async {
    print("INFO - attempting login of ${username}");
    HttpClientResponse response = await command("Key=vtku&Command=Logon&Username=${username}&Password=" + Uri.encodeFull(password));
    if (response.statusCode == 200) {
      var loginXml = await utf8.decodeStream(response);
      Login login = KamarAdapter.transformLogin(loginXml);
      _login = login;
      if (login.errorCode == 0 && login.success) {
        this.key = login.key;
        this.studentId = login.studentId;

        String calendarXml = await calendar();
        if (calendarXml != null) {
          String timetableXml = await timetable();
          if (timetableXml != null) {
            DayList dayList = KamarAdapter.transform(calendarXml, timetableXml);

            LoginSettings ls = new LoginSettings((b) => b
              ..username = username
              ..password = password
              ..formTeacher = dayList.formTeacher
              ..level = dayList.level
              ..studentId = studentId
            );

            // save login details
            await Future.wait([
              db.saveLoginSettings(ls),
              db.updateSubjects(dayList.subjects),
              db.updateDays(dayList.days),
              db.updateTerms(dayList.terms)]);

            print('SAVED DATA');

            return true;
          } else {
            print('ERROR - could not get timetable');
          }
        } else {
          print("ERROR - could not get calendar");
        }
      } else {
        print("ERROR - 200 success, login failed, ${login}");
      }
    } else {
      print("ERROR - failed login, error code ${response.statusCode}");
    }

    return false;
  }
  

  Future<String> calendar() async {
    int year = new DateTime.now().year;
    HttpClientResponse response = await command("Key=${key}&Command=GetCalendar&Year=${year}");
    if (response.statusCode == 200) {
      return await UTF8.decodeStream(response);
    } else {
      return null;
    }
  }

  Future<String> timetable() async {
    int year = new DateTime.now().year;
    HttpClientResponse response = await command("Key=${key}&Command=GetStudentTimetable&StudentID=${studentId}&Grid=${year}TT");
    if (response.statusCode == 200) {
      return await UTF8.decodeStream(response);
    } else {
      return null;
    }
  }

  String getLastLoginErrorMessage() {
    if (_login == null || _login.error == null) {
      return '';
    } else {
      return _login.error;
    }
  }

}
