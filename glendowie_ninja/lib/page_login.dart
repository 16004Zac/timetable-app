import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/kamar_api.dart';
import 'package:glendowie_ninja/models.dart';

class NinjaLoginPage extends StatefulWidget {
  final KamarApi api;
  final NinjaDatabase db;
  final FirebaseAnalytics analytics;

  NinjaLoginPage({Key key, this.api, this.db, this.analytics}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title = 'Glendowie College Timetable Ninja';

  @override
  _NinjaLoginPageState createState() => new _NinjaLoginPageState();
}

class _NinjaLoginPageState extends State<NinjaLoginPage> {
  bool busy = false;
  final TextEditingController usernameController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();
  final GlobalKey<ScaffoldState> loginKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final FocusNode passwordFocusNode = new FocusNode();

  void showSnacks(String text) {
    loginKey.currentState.showSnackBar(new SnackBar(content: new Text(text)));
  }

  // try and load their info, if we can

  @override
  void initState() {
    widget.analytics.setCurrentScreen(screenName: 'login');

    widget.db.getLoginSettings().then((ls) {
      if (ls != null) {
        usernameController.text = ls.studentId;
        passwordController.text = ls.password;
      }
    });
  }

  void _login() {
    // don't let them keep hitting it
    if (busy || !formKey.currentState.validate()) {
      return;
    }

    var uname = usernameController.text.trim();
    var pword = passwordController.text.trim();

    if (uname.length > 0 && pword.length > 0) {
      setState(() {
        busy = true;
        widget.api.login(uname, pword).then((success) {
          if (success) {
            Navigator.popUntil(context, ModalRoute.withName('/'));
          } else {
            showSnacks(
                'Unable to login ' + widget.api.getLastLoginErrorMessage());
          }
        }).whenComplete(() {
          setState(() => busy = false);
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var backgroundImage = new Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
          colorFilter: new ColorFilter.mode(
              Colors.black.withOpacity(0.2), BlendMode.dstATop),
          image: new AssetImage('assets/gc-landscape.jpeg'),
          fit: BoxFit.cover,
        ),
      ),
    );

    var col = new ListView(
      children: <Widget>[
        new Container(
          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
          child:         new Container(
              height: 73.0,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage('assets/glendowie_logo.png'),
                  fit: BoxFit.fitHeight,
                ),
              )),

        ),
        _buildForm(context),
      ],
    );

    return new Scaffold(
      key: loginKey,
      body: new SafeArea(
        child: new Stack(children: <Widget>[
          backgroundImage,
          new Positioned.fill(child: col)
        ])
      ));
  }

  Widget _buildForm(BuildContext context) {
    var usernameField = new TextFormField(
        autocorrect: false,
        keyboardType: TextInputType.text,
        controller: usernameController,
        validator: (val) => val.isEmpty? 'Username can\'t be empty.' : null,
        onFieldSubmitted: (val) {
          FocusScope.of(context).requestFocus(passwordFocusNode);
        },
        decoration: const InputDecoration(
          icon: const Icon(Icons.person),
          hintText: 'What is your student id?',
          labelText: 'Student ID *',
        ));

    var formChildren = <Widget>[];

    formChildren.add(usernameField);

    formChildren.add(new TextFormField(
        keyboardType: TextInputType.text,
        focusNode: passwordFocusNode,
        controller: passwordController,
        validator: (val) => val.isEmpty? 'Password can\'t be empty.' : null,
        onFieldSubmitted: (v) {
          if (usernameController.text.isNotEmpty && passwordController.text.isNotEmpty) {
            _login();
          }
        },
        decoration: const InputDecoration(
          icon: const Icon(Icons.lock),
          hintText: 'What is your password?',
          labelText: 'Password *',
        ),
        obscureText: true));

    if (busy) {
      formChildren.add(new LinearProgressIndicator(value: null));
    }

    formChildren.add(new Container(
      padding: const EdgeInsets.all(20.0),
      alignment: Alignment.center,
      child: new RaisedButton(
        child: const Text('LOGIN'),
        onPressed: busy ? null : _login,
      ),
    ));
    
    return  new Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: new Form(
          key: formKey,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
              children: formChildren)),
    );
  }
}
