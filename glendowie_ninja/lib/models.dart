
library ninja_model;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'models.g.dart';

abstract class LoginSettings implements Built<LoginSettings, LoginSettingsBuilder> {
  static Serializer<LoginSettings> get serializer => _$loginSettingsSerializer;

  String get username;
  String get password;

  @nullable
  String get schoolUrl;

  @nullable
  String get schoolName;

  @nullable
  String get schoolLogo;

  // below normally appear after we have logged in
  @nullable
  String get studentId;

  @nullable
  String get level; // what year (5 - 13)

  @nullable
  String get formTeacher;

  factory LoginSettings([updates(LoginSettingsBuilder b)]) = _$LoginSettings;

  LoginSettings._();
}

abstract class Period implements Built<Period, PeriodBuilder> {
  static Serializer<Period> get serializer => _$periodSerializer;

  int get slot;
  String get teacher;
  String get subject;
  String get room;

  factory Period([updates(PeriodBuilder b)]) = _$Period;
  
  Period._();
}

class DayStatusType extends EnumClass {
  static Serializer<DayStatusType> get serializer => _$dayStatusTypeSerializer;

  static const DayStatusType open = _$open;
  static const DayStatusType weekend = _$weekend;
  static const DayStatusType holiday = _$holiday;

  const DayStatusType._(String name) : super(name);

  static BuiltSet<DayStatusType> get values => _$dstValues;
  static DayStatusType valueOf(String name) => _$dstValueOf(name);
}

abstract class Day implements Built<Day, DayBuilder> {
  static Serializer<Day> get serializer => _$daySerializer;

  int get week;
  int get dayOfWeek;
  int get term;
  int get termWeek;
  DateTime get day;
  DayStatusType get dayStatusType;
  @nullable
  BuiltSet<Period> get periods;

  factory Day([updates(DayBuilder b)]) = _$Day;

  Day._();
}

abstract class Terms implements Built<Terms, TermsBuilder> {
  static Serializer<Terms> get serializer => _$termsSerializer;

  BuiltMap<String,int> get terms;

  factory Terms([updates(TermsBuilder b)]) = _$Terms;

  Terms._();
}

abstract class SubjectSettings implements Built<SubjectSettings, SubjectSettingsBuilder> {
  static Serializer<SubjectSettings> get serializer => _$subjectSettingsSerializer;

  String get name;
  int get backgroundColour;
  int get foregroundColour;

  factory SubjectSettings([updates(SubjectSettingsBuilder b)]) = _$SubjectSettings;

  SubjectSettings._();
}

abstract class Subject implements Built<Subject, SubjectBuilder> {
  static Serializer<Subject> get serializer => _$subjectSerializer;

  String get name;

  BuiltSet<DateTime> get days;

  factory Subject([updates(SubjectBuilder b)]) = _$Subject;

  Subject._();
}

abstract class Homework implements Built<Homework, HomeworkBuilder> {
  static Serializer<Homework> get serializer => _$homeworkSerializer;

  String get id;

  DateTime get whenDue;
  String get subject;
  String get title;
  String get description;
  int get homeworkSize;  //
  int get value;

  @nullable
  bool get completed;

  factory Homework([updates(HomeworkBuilder b)]) = _$Homework;

  Homework._();
}
