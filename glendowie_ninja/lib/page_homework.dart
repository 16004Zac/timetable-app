import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/component_drawer.dart';
import 'package:glendowie_ninja/models.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

GoogleSignIn _googleSignIn = new GoogleSignIn();

class NinjaHomeworkViewPage extends StatefulWidget {
  final NinjaDatabase db;
  final FirebaseAnalytics analytics;

  NinjaHomeworkViewPage({Key key, this.db, this.analytics}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _NinjaHomeworkViewPageState();
  }
}

class _NinjaHomeworkViewPageState extends State<NinjaHomeworkViewPage> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  List<SubjectSettings> _subjectSettings = [];
  Map<String, SubjectSettings> _subs = {};
  GoogleSignInAccount _currentUser;
  PageController pageController;
  Map<int, List<Homework>> homeworkCache = {};
  String _selectedId;

  @override
  void initState() {
    super.initState();

    pageController = new PageController(initialPage: 1);

    // check if they haven't logged in before and send them there
    widget.db.loginState().then((loggedIn) {
      if (!loggedIn) {
        Navigator.pushNamed(context, '/login').then((s) => _login());
      } else {
        _login();
      }
    });
  }

  void _login() {
    widget.analytics.setCurrentScreen(
        screenName: 'yourhomework', screenClassOverride: 'general');

    widget.db.getFullSubjectSettings().then((vals) {
      setState(() {
        _subs.clear();
        vals.forEach((v) => _subs[v.name] = v);
        print("_subs is ${_subs}");
        _subjectSettings = vals;
      });
    });

    // get permission to their gdrive
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      print("returned");
      setState(() {
        _currentUser = account;
      });
    });
  }

  Future<FirebaseUser> _handleSignIn() async {
    GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    FirebaseUser user = await _auth.signInWithGoogle(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    print("signed in " + user.displayName);
    return user;
  }

  AppBar _buildAppBar() {
    return new AppBar(
      elevation: 0.0,
      title: new Text('Your Homework'),
//      actions: <Widget>[
//        new IconButton(icon: const Icon(Icons.insert_drive_file), onPressed: () {
//          _googleSignIn.isSignedIn().then((signedIn) {
//            if (!signedIn) {
//              _handleSignIn();
//            }
//          });
//        }),
//      ],
    );
  }

  void _requestCurrentHomework() {
    widget.db
        .getCurrentHomework()
        .then((hw) => setState(() => this.homeworkCache[1] = hw));
  }

  void _requestOldHomework() {
    widget.db
        .getOldHomework()
        .then((hw) => setState(() => this.homeworkCache[0] = hw));
  }

  Widget createHomeworkRow(Color foreground, Color background, Homework hw) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: GestureDetector(
        onTap: () {
          setState(() {
            if (_selectedId == hw.id) {
              _selectedId = null;
            } else {
              _selectedId = hw.id;
            }
          });
        },
        child: Container(
            decoration: new ShapeDecoration(
              color: background,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.all(Radius.circular(8.0)),
                )
            ),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 10.0, left: 8.0, top: 8.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                      width: 80.0,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: new Container(
                          alignment: AlignmentDirectional.topStart,
                          child: new Text(
                            hw.subject,
                            style: new TextStyle(color: foreground),
                          ),
                        ),
                      )),
                  Flexible(
                      child: Text(
                    hw.title,
                    style: new TextStyle(color: foreground),
                  ))
                ],
              ),
            )),
      ),
    );
  }

  String _dueString(DateTime whenDue) {
    return whenDue.month.toString().padLeft(2, '0') +
        '-' +
        whenDue.day.toString().padLeft(2, '0');
  }

  Widget createDayDueHomework(List<Widget> children, DateTime whenDue) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Align(
            alignment: Alignment.topLeft,
              child: new Text("${_dueString(whenDue)}",
                  style: TextStyle(fontWeight: FontWeight.bold))),
        ),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 12.0),
          child: Column(
            children: children,
          ),
        ))
      ],
    );
  }


  int lastIndex = -1;

  Widget createSubjectHomeworkView(BuildContext context, int index) {
    String text;

    if (index == 0) {
      text = 'Completed';
      if (lastIndex != index) {
        _requestOldHomework();
      }
    } else if (index == 1) {
      text = 'Active';
      if (lastIndex != index) {
        _requestCurrentHomework();
      }
    } else {
      text = _subjectSettings[index - 2].name;
    }

    lastIndex = index;

    List<Widget> items = [
      Center(
        child: new Text(
          text,
          style: new TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
        ),
      )
    ];

    List<Widget> columnChildren = [];
    DateTime lastDay = null;

    List<Homework> homework = homeworkCache[index];

    if (homework != null) {
      homework.forEach((hw) {
        Color foreground = _subs[hw.subject] == null
            ? Colors.blueGrey
            : new Color(_subs[hw.subject].foregroundColour);
        Color background = _subs[hw.subject] == null
            ? Colors.white
            : new Color(_subs[hw.subject].backgroundColour);

        Widget childObject = null;

        if (hw.id == _selectedId) {
          childObject = Padding(
            padding: const EdgeInsets.only(bottom: 30.0),
            child: new Column(
              children: <Widget>[
                createHomeworkRow(foreground, background, hw),
                description(hw, background, foreground)
              ],
            ),
          );
        } else {
          childObject = createHomeworkRow(foreground, background, hw);
        }

        if (lastDay != null && lastDay.difference(hw.whenDue).inDays != 0) {
          items.add(createDayDueHomework(columnChildren, lastDay));
          columnChildren = [];
        }
        columnChildren.add(childObject);

        lastDay = hw.whenDue;
      });

      if (columnChildren.length > 0) {
        items.add(createDayDueHomework(columnChildren, lastDay));
      }
    }

    return new ListView(
      children: items,
    );
  }

  Widget description(Homework hw, Color background, Color foreground) {
    var side = BorderSide(color: background);
    return Container(
      decoration: BoxDecoration(
        border: new Border(left: side, bottom: side, right: side)
      ),
      child:  Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                children: <Widget>[
                  Expanded(child: new Text(hw.description))
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child:Text("Credits ${hw.value}")
                ),
                 SizedBox(width: 80.0,
                    child: FlatButton(onPressed: (){},
                      color: background,
                      child: Text("EDIT", style: TextStyle(color: foreground)),
                    )
                  ),
              ],
            )
          ],
        ),
      )
    );
  }

  Widget body() {
    return new PageView.builder(
      controller: pageController,
      itemBuilder: (BuildContext context, int index) =>
          createSubjectHomeworkView(context, index),
      itemCount: _subjectSettings.length + 2,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: _buildAppBar(),
      drawer: new NinjaDrawer(widget.db),
      body: body(),
      floatingActionButton: new FloatingActionButton(
          child: new Icon(Icons.add),
//          backgroundColor: new Color(0xFFE57373),
          onPressed: () {
            Navigator.pushNamed(context, '/new_homework');
          }),
    );
  }
}
