import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/models.dart';
import 'package:glendowie_ninja/time_peroids.dart';

class DayView extends StatelessWidget {
  final DateTime day;
  final Day _dayData;
  final TimePeriods timePeriods;

  DayView(this.day, this._dayData, this.timePeriods);

  String timeDiff(DateTime when) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    return '${twoDigits(when.hour)}:${twoDigits(when.minute)}';
  }

  @override
  Widget build(BuildContext context) {
    var timeAsString = timeDiff(new DateTime.now());

    var children = <Widget>[];
    if (_dayData == null) {
      children.add(new Center(
          child: new Text('Free Study Day!'))); // else we are waiting
    } else if (_dayData.dayStatusType != DayStatusType.open ||
        _dayData.periods == null ||
        _dayData.periods.length == 0) {
      children.add(new Center(
          child: new Text(
              'School not open today (${_dayData.dayStatusType.toString()})')));
    } else {
      ConfiguredTimeCollection time = timePeriods.findWeekDay(_dayData.day);

      var thinBlueLine = new SizedBox(
        height: 5.0,
        child: new Container(
          color: Colors.blue,
        ),
      );

      int periodSlot = 0;
      for (int pos = 0; pos < time.range.length; pos++) {
        var range  =time.range[pos];
        if (range.isSpacer()) {
          children.add(new ListTile(
              title: new Row(
                children: <Widget>[
                  new Padding(
                      padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                      child: new Text(time.range[pos].name)),
                  new Text('')
                ],
              )
          ));
        } else if (periodSlot < _dayData.periods.length) {
          var period = _dayData.periods.elementAt(periodSlot);
          var timeSlot = time.range[pos];
          String desc =
              timeSlot.start.toString() + ' - ' + timeSlot.end.toString();

          if (timeAsString.compareTo(timeSlot.end.toString()) < 0 && thinBlueLine != null) {
            children.add(thinBlueLine);
            thinBlueLine = null;
          }

          children.add(new ListTile(
            title: new Row(
              children: <Widget>[
                new Padding(
                    padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                    child: new Text(desc)),
                new Text(period.subject)
              ],
            ),
            subtitle: new Row(
              children: <Widget>[
                new Padding(
                    padding: new EdgeInsets.only(left: 8.0, right: 8.0),
                    child: new Text('Room ' + period.room)),
                new Text(period.teacher)
              ],
            ),
          ));
          periodSlot ++;
        }
      }
    }

    return new ListView(
      children: children,
    );
  }
}
