import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/kamar_api.dart';
import 'package:glendowie_ninja/ninja_subject.dart';
import 'package:glendowie_ninja/page_new_homework.dart';
import 'package:glendowie_ninja/page_settings.dart';
import 'package:glendowie_ninja/time_peroids.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

import 'page_dayview.dart';
import 'page_weekview.dart';
import 'page_homework.dart';
import 'page_login.dart';

void main() async {
  NinjaDatabase db = new NinjaDatabase();
  KamarApi api = new KamarApi('https://glendowie.mystudent.school.nz', db);

  db.getLoginSettings().then((ls) =>
      runApp(new TimetableNinja(api, db,
          new GlendowieTime(), ls == null ? '/login' : '/')));
}

final FirebaseAnalytics analytics = new FirebaseAnalytics();

class TimetableNinja extends StatelessWidget {

  final NinjaDatabase db;
  final KamarApi api;
  final TimePeriods timePeriods;
  final String initialRoute;

  TimetableNinja(this.api, this.db, this.timePeriods, this.initialRoute);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Glelndowie College Timetable Ninja',
      onGenerateRoute: _generateRoue,
//      navigatorObservers: [
//        new FirebaseAnalyticsObserver(analytics: analytics),
//      ],
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: <String, WidgetBuilder> {
        '/': (BuildContext context) => new NinjaDayViewPage(db: db, timePeriods: timePeriods, analytics: analytics,),
        '/weekview': (BuildContext context) => new NinjaWeekViewPage(db: db, timePeriods: timePeriods, analytics: analytics,),
        '/homeworkview': (BuildContext context) => new NinjaHomeworkViewPage(db: db, analytics: analytics,),
        '/login': (BuildContext context) => new NinjaLoginPage(api: api, db: db, analytics: analytics,),
        '/settings': (BuildContext context) => new NinjaSettingsPage(db : db, analytics: analytics),
        '/new_homework': (BuildContext context) => NinjaNewHomeworkViewPage(db : db, analytics: analytics),
      },
      initialRoute: initialRoute,
    );
  }

  Route<Null> _generateRoue(RouteSettings settings) {
    if (settings.name.startsWith('/subject/')) {
      return new MaterialPageRoute<Null>(
          builder: (context) =>
            new NinjaSubjectPage(db, settings.name.substring('/subject/'.length)),
          settings: settings);
    }

    return null;
  }
}



