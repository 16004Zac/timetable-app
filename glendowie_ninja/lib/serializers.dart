
library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:glendowie_ninja/models.dart';

part 'serializers.g.dart';

/// Collection of generated serializers for the built_value chat example.
@SerializersFor(const [
  Day,
  Period,
  LoginSettings,
  Subject,
  SubjectSettings,
  Terms,
  Homework
])
final Serializers serializers = _$serializers;