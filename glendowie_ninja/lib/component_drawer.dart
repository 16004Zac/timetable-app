
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/models.dart';

class NinjaDrawer extends StatefulWidget {
  final NinjaDatabase db;

  NinjaDrawer(this.db);

  @override
  State<StatefulWidget> createState() {
    return new _NinjaDrawerState();
  }
}

class _NinjaDrawerState extends State<NinjaDrawer> {
  List<String> subjects = [];
  LoginSettings loginSettings;

  _NinjaDrawerState() {
  }

  @override
  void initState() {
    widget.db.getSubjects().then((lSubjects) {
      widget.db.getLoginSettings().then((lSettings) {
        setState(() {
          subjects = lSubjects;
          loginSettings = lSettings;
        });
      });
    });
  }

  void _handleShowAbout() {
    Navigator.pushNamed(context, '/about');
  }

  void _handleShowSettings() {
    Navigator.pushNamed(context, '/settings');
  }

  void _handleRefresh() {
    Navigator.pushNamed(context, '/login');
  }

  Drawer _buildDrawer() {

    List<Widget> children = [
      new DrawerHeader(
          decoration:
          new BoxDecoration(

              image: new DecorationImage(image: new AssetImage('assets/gc-logo.jpeg'), fit: BoxFit.cover)
          ),
          child: new Align(
              alignment: Alignment.bottomLeft,
              child: new Text(loginSettings == null ?
              '' : loginSettings.studentId + ' (' + loginSettings.formTeacher + ')',
                  style: new TextStyle(color: new Color.fromRGBO(255, 255, 255, 1.0), fontWeight: FontWeight.bold, fontSize: 24.0))

          )),
    ];

    children.add(
        new ListTile(
          dense: true,
          leading: const Icon(Icons.add),
          title: const Text('Your Day'),
          onTap: () => Navigator.popAndPushNamed(context, '/'),
        ));

    children.add(
        new ListTile(
          dense: true,
          leading: const Icon(Icons.hotel),
          title: const Text('Your Week'),
          onTap: () => Navigator.popAndPushNamed(context, '/weekview'),
        ));

    children.add(
        new ListTile(
          dense: true,
          leading: const Icon(Icons.hot_tub),
          title: const Text('Your Homework'),
          onTap: () => Navigator.popAndPushNamed(context, '/homeworkview'),
        ));


    children.add(const Divider());
    children.add(
        new ListTile(
          dense: true,
          leading: const Icon(Icons.settings),
          title: const Text('Refresh'),
          onTap: _handleRefresh,
        ));
    children.add(
      new ListTile(
        dense: true,
        leading: const Icon(Icons.help),
        title: const Text('About'),
        onTap: _handleShowAbout,
      ),
    );

    return new Drawer(
      child: new ListView(
          children: children
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildDrawer();
  }
}