
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/models.dart';

class NinjaSettingsPage extends StatefulWidget {
  final NinjaDatabase db;
  final FirebaseAnalytics analytics;

  const NinjaSettingsPage({Key key, this.db, this.analytics}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _NinjaSettingsState();
  }

}

class _NinjaSettingsState extends State<NinjaSettingsPage> {
  List<SubjectSettings> subjects = [];

  @override
  Widget build(BuildContext context) {
    List<Widget> rows = [];

    rows.add(new Text('Subject Colours', style: new TextStyle(fontWeight: FontWeight.bold)));

    subjects.forEach((s) {
      rows.add(new Text(s.name));
    });

    return Column(children: rows,);
  }

  @override
  void initState() {
    super.initState();

    widget.db.getLoginSettings().then((loggedIn) {
      if (loggedIn == null) {
        Navigator.pushNamed<LoginSettings>(context, '/login').then((ls) {
          _setupLoggedIn(ls);
        });
      } else {
        _setupLoggedIn(loggedIn);
      }
    });
  }

  void _setupLoggedIn(LoginSettings loginSettings) {
    widget.db.getFullSubjectSettings().then((su) => setState(() => subjects = su));
  }

}