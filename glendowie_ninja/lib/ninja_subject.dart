import 'package:flutter/material.dart';
import 'package:glendowie_ninja/db.dart';
import 'package:glendowie_ninja/kamar_api.dart';
import 'package:glendowie_ninja/models.dart';
import 'package:glendowie_ninja/component_drawer.dart';
import 'package:intl/intl.dart';


class NinjaSubjectPage extends StatefulWidget {
  final NinjaDatabase db;
  final String subject;

  NinjaSubjectPage(this.db, this.subject);

  @override
  State<StatefulWidget> createState() {
    return new _NinjaSubjectPageState();
  }
}

class _NinjaSubjectPageState extends State<NinjaSubjectPage> {
  Subject subjectData;

  @override
  void initState() {
    super.initState();
    widget.db.getSubject(widget.subject).then((s) {
      setState(() => subjectData = s);
    });
  }

  AppBar _buildAppBar() {
    return new AppBar(
      elevation: 0.0,
      title: new Text("Subject ${widget.subject}"),
    );
  }

  @override
  Widget build(BuildContext context) {
    var content = new FutureBuilder(
      future: widget.db.getSubject(widget.subject),
      builder: (BuildContext context, AsyncSnapshot<Subject> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Center(child: new Text('loading....'));
          default:
            if (snapshot.hasError) {
              return new Center(child: new Text('failed to load subject'));
            } else if (snapshot.hasData) {
              return _createListView(context, snapshot);
            }
        }
      }
    );
    
    return new Scaffold(
        appBar: _buildAppBar(),
        drawer: new NinjaDrawer(widget.db),
        body: content
    );
  }

  Widget _createListView(BuildContext context, AsyncSnapshot<Subject> snapshot) {
    DateFormat df = new DateFormat('EEE, d/M/y');
    return new ListView.builder(
      itemCount: snapshot.data.days.length,
      itemBuilder: (BuildContext context, int index) {
        return new Row(
          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.symmetric(vertical: 16.0),
              child: new Material(
                child: new Text(
                  df.format(snapshot.data.days.elementAt(index))
        )))],
        );
      });
  }

}