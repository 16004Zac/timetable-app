# glendowie_ninja

A Kamar extension for Glendowie College

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).

## Model generation

This uses the Built Value project: https://github.com/google/built_value.dart 

To get the models to build and generate, you need to use ensure you set the FLUTTER_ROOT environment variable,

`export FLUTTER_ROOT=$FLUTTER`

and then you can run 

`pub run build_runner watch` - for continuous build

or

`pub run build_runner build` - for one off build

It doesn't output standard JSON - its a weird list format. So you need to add a standard json
plugin if you want to write standard json. In this case we don't as its for internal use only.

https://medium.com/dartlang/moving-fast-with-dart-immutable-values-1e717925fafb

== release Android

- update version in android/app/build.gradle - the versionName property and versionCode

`flutter build apk`
